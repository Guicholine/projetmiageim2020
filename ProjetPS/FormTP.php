<?php
require 'fonctions.php';
session_start();
  
  $session=connexionbd();
  if (!(isset($_SESSION['typeuser']) and isset($_SESSION["login"]) and $_SESSION['typeuser']=="admin" and $_SESSION['login']=="admin")) {
  header("location:index.php"); 
}
else {
?>
<!DOCTYPE HTML>

<html>

<head>

  <title> Créer un nouveau TP </title>
  <meta charset="utf-8">
    <link rel="stylesheet" href="css/bootstrap/bootstrap.css">



</head>


<body>
        <h3><center>Ajouter un nouveau groupe de TP</center></h3>
        </br>
        </br>

    <div class="col-md-9">

      <div class="container">
        <div class="row">
          <fieldset style="width: 500px">
  <form method="POST" action="addTP.php" onsubmit="if(!confirm('Confirmez-vous cette action ?')){
          return false;}">

   <label for="NomTP"> Veuillez donner un nom au groupe de TP : </label>
   <input type="text" name ="NomTP" required="true">
<div class="row">
                <div class="col-md-12">
   <label>Séléctionnez le groupe de TD correspondant :</label>
   <select name='TD' id='TD'>
    <?php
      $session=connexionbd();
      $res = mysqli_query($session,"SELECT * FROM groupestd gtd,sections s where s.numSec=gtd.numSec");
      while($row = mysqli_fetch_assoc($res)){
        echo "<option value='".$row["numTD"]."'>".$row["numSem"]."-".$row["nomSec"]."-".$row["nomTD"]."</option>";
      }
    
    ?>
  </select>
</div>
                <div class="col-md-12">
  <input type="submit" value="Créer le nouveau TP">
</div>
              </div>


</form>
</fieldset>
        </div>
      </div>
    </div>

</body>



</html>
<?php
}
?>
