<?php
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

function connexionbd(){
    $session = mysqli_connect('localhost', 'root', ''); 
    if ($session == NULL) { // Test de connexion réussie
      echo ("<p>Echec de connection</p>");
    } 
    else {
        // Sélection de la base de donnée
        if (mysqli_select_db($session, 'projetpaulsab') == TRUE) { 
            mysqli_set_charset($session, "utf8");
            return $session;
        }
        else{
                echo ("Cette base n'existe pas</br>");
        }  
    }
}

function connexionetudiant($session,$login,$mdp){
    if(isset($login)){
        $stmt = mysqli_prepare($session, "SELECT mdp FROM etudiants WHERE numEtu=?");
        mysqli_stmt_bind_param($stmt, "s", $login);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $good_password);
        mysqli_stmt_fetch($stmt);
        if(!password_verify($mdp,$good_password)) {
            echo '<p>Mauvais login / password. Merci de recommencer</p>';
            echo '<input type = "button" value = "Réessayez de vous connecter"  onclick = "history.back()">';
        }
        else {
            session_start();
            $_SESSION['login'] = $login;
            $_SESSION['password'] = $$mdp;
            $_SESSION['typeuser'] = 'etu';
            header('Location: menu.php');
        }   
    }
}


function connexionens($session,$login,$mdp){
    if(isset($login)){
  
    $stmt = mysqli_prepare($session, "SELECT mdp FROM enseignants WHERE numEns=?");
    mysqli_stmt_bind_param($stmt, "s", $login);
    mysqli_stmt_execute($stmt);

     mysqli_stmt_bind_result($stmt, $good_password);
     mysqli_stmt_fetch($stmt);
    
    if(!password_verify($mdp,$good_password)) {
        echo '<p>Mauvais login / password. Merci de recommencer</p>';
        echo '<input type = "button" value = "Réessayez de vous connecter"  onclick = "history.back()">';
           }
  else {
    session_start();
    
    $_SESSION['login'] = $login;
    $_SESSION['password'] = $$mdp;
    $_SESSION['typeuser'] = 'ens';


    
header('Location: menu.php');

  }   
}
}

function connexionadmin($session,$login,$mdp){
    if($login=="admin" and $mdp=="pass"){
        session_start();
        $_SESSION['login'] = 0000;
        $_SESSION['password'] = $mdp;
        $_SESSION['typeuser'] = 'admin';
        header('Location: menu.php');
    }
    else{
        echo '<p>Mauvais login / password. Merci de recommencer</p>';
        echo '<input type = "button" value = "Réessayez de vous connecter"  onclick = "history.back()">';
    }
  
}



function afficheColExp($session,$ne){
    $fille=recupcolonnesfilles($session,$ne);
    $mere=recupcolonnesmere($session,$ne);
    echo "<tr>";
     while ($ligne=mysqli_fetch_array ($mere)) {
        $nbfille=combiendesouscolonnepourunemere($session,$ligne["numCol"]);

        echo '<td  colspan="'.$nbfille.'">'.$ligne["libelle"].'</td>';
    }
    echo"</tr>";
    echo "<tr>";
     while ($ligne=mysqli_fetch_array ($fille)) {
        echo '<td>'.$ligne["libelle"].'</td>';
    }
    echo"</tr>";
}

function afficheFormulaireRemplit($session,$ne){
    $fille=recupcolonnesfilles($session,$ne);
    $mere=recupcolonnesmere($session,$ne);
    $i=1;
    echo "<tr>";
     while ($ligne=mysqli_fetch_array ($mere)) {
        $nbfille=combiendesouscolonnepourunemere($session,$ligne["numCol"]);

        echo '<td  colspan="'.($nbfille*2).'">'.$ligne["libelle"].'</td>';
    }
    echo"</tr>";
    echo "<tr>";
     while ($ligne=mysqli_fetch_array ($fille)) {
        echo '<td>'.$ligne["libelle"].'</td>';
        echo "<td> <input type='text' id='numCol' name='".$ligne["numCol"]."'> </td>";
            $i++;
    }
    echo"</tr>";
    echo "<tr>";
}

//renvoi le nombre de colonnes mere pour une experience
function combiendecolmerepouruneexp($session,$ne){
    $query = 'SELECT * FROM colonnes WHERE  numColMere is NULL and numExp='.$ne.'';
    $result = mysqli_query ($session, $query); 
    return mysqli_num_rows($result);
}

//renvoi le nombre de sous-colonnes pour une colonne mere 
function combiendesouscolonnepourunemere($session,$ncolmere){
    $query = 'SELECT * FROM colonnes WHERE  numColMere='.$ncolmere.'';
    $result = mysqli_query ($session, $query);
    return mysqli_num_rows($result);
}

//renvoi le nombre de sous-colonnes pour une experience
function combiendesouscolonnepouruneexperience($session,$ne){
    $query = 'SELECT * FROM colonnes WHERE  numExp='.$ne.'and numColMere is not NULL';
    $result = mysqli_query ($session, $query); 
    return mysqli_num_rows($result);
}

//renvoi le dernier numéro de colonne entrée
function quelmaxdecol($session){
    $stmt = mysqli_prepare($session, "SELECT max(numCol) from colonnes");
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $maxcol);
    mysqli_stmt_fetch($stmt);
    return $maxcol;
}

// renvoi le numéro d'une section selon son nom
function quelnumsec($session,$noms){
    $query ="SELECT numSec from sections where nomSec=".$noms."";
    $result = mysqli_query ($session, $query); 
    return $result;
}


//Suppression de section
function deletesection($session,$nsec){
    $stmt = mysqli_prepare($session, 'DELETE FROM sections WHERE sections.numSec = ?');
    mysqli_stmt_bind_param($stmt,"i",$nsec);
    mysqli_stmt_execute($stmt);
}

//Suppression de TD
function deleteTD($session,$ntd){
    $stmt = mysqli_prepare($session, 'DELETE FROM groupestd WHERE groupestd.numTD = ?');
    mysqli_stmt_bind_param($stmt,"i",$ntd);
    mysqli_stmt_execute($stmt);
}

//Suppression de TP
function deleteTP($session,$ntp){
    $stmt = mysqli_prepare($session, 'DELETE FROM groupestp WHERE groupestp.numTP = ?');
    mysqli_stmt_bind_param($stmt,"i",$ntp);
    mysqli_stmt_execute($stmt);
}

//Suppression d'Etudiants
function deleteetudiant($session,$netu){
    $stmt = mysqli_prepare($session, 'DELETE FROM etudiants WHERE etudiants.numEtu = ?');
    mysqli_stmt_bind_param($stmt,"i",$netu);
    mysqli_stmt_execute($stmt);
}

//Suppression d'Enseignants
function deleteenseignant($session,$nens){
    $stmt = mysqli_prepare($session, 'DELETE FROM enseignants WHERE enseignants.numEns = ?');
    mysqli_stmt_bind_param($stmt,"i",$nens);
    mysqli_stmt_execute($stmt);
}

//Suppression d'Experiences
function deleteexperience($session,$nexp){
    $stmt = mysqli_prepare($session, 'DELETE FROM experiences WHERE experiences.numExp = ?');
    mysqli_stmt_bind_param($stmt,"i",$nexp);
    mysqli_stmt_execute($stmt);
}

//Suppression de Colonnes
function deletecolonne($session,$ncol){
    $stmt = mysqli_prepare($session, 'DELETE FROM colonnes WHERE colonnes.numCol = ?');
    mysqli_stmt_bind_param($stmt,"i",$ncol);
    mysqli_stmt_execute($stmt);
}

//Suppression de Données dans remplit
function deleteremplit($session,$netu,$ncol){
    $stmt = mysqli_prepare($session, 'DELETE FROM remplit WHERE remplit.numEtu = ? and remplit.numCol=?');
    mysqli_stmt_bind_param($stmt,"ii",$netu,$ncol);
    mysqli_stmt_execute($stmt);
}

//Inserer une nouvelle Donnée dans remplit
function insertremplit($session,$netu,$ncol,$d){
    $stmt = mysqli_prepare($session, 'INSERT INTO remplit(numEtu,numCol,donnees) VALUES(?,?,?)');
    mysqli_stmt_bind_param($stmt,"iii",$netu,$ncol,$d);
    mysqli_stmt_execute($stmt);

}
//Inserer une nouvelle experience
function insertexperience($session,$nens,$nome,$datef,$resum,$numsem){
    $stmt = mysqli_prepare($session, 'INSERT INTO experiences (titre,dateDeb,dateFin,resume,etat,numEns,numSem) VALUES(?,NOW(),?,?,"Ouvert",?,?)');
    mysqli_stmt_bind_param($stmt,"sssii",$nome,$datef,$resum,$nens,$numsem);
    mysqli_stmt_execute($stmt);
}

//renvoi le numéro de la derniere experience entrée
function maxexp($session){
    $stmt = mysqli_prepare($session, "SELECT max(numExp) from experiences");
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $maxexp);
    mysqli_stmt_fetch($stmt);
    return $maxexp;
}
//Inserer une nouvelle colonne
function insertcolonne($session,$nexp,$lib,$type){
   $stmt = mysqli_prepare($session, 'INSERT INTO colonnes(libelle,typeDonnees,numExp) VALUES(?,?,?)');
    mysqli_stmt_bind_param($stmt,"sii",$lib,$type,$nexp);
    mysqli_stmt_execute($stmt);
    
}

//Inserer une nouvelle colonne fille
function insertcolonnefille($session,$nexp,$lib,$type,$numcolmere){
    $stmt = mysqli_prepare($session, 'INSERT INTO colonnes(libelle,typeDonnees,numExp,numColMere) VALUES(?,?,?,?)');
    mysqli_stmt_bind_param($stmt,"siii",$lib,$type,$nexp,$numcolmere);
    mysqli_stmt_execute($stmt);
}


//Inserer un nouvel etudiant
function insertetudiant($session,$netu,$mdp,$ntp,$nom,$prenom){
   $stmt = mysqli_prepare($session, 'INSERT INTO etudiants(numEtu,nomEtu,prenomEtu,mdp,numTP) VALUES(?,?,?,?,?)');
    mysqli_stmt_bind_param($stmt,"isssi",$netu,$nom,$prenom,$mdp,$ntp);
    mysqli_stmt_execute($stmt);
}

//Inserer un nouvel enseignant
function insertenseignant($session,$nens,$mdp,$nom,$prenom){
   $stmt = mysqli_prepare($session, 'INSERT INTO enseignants(numEns,nomEns,prenomEns,mdp) VALUES(?,?,?,?)');
    mysqli_stmt_bind_param($stmt,"isss",$nens,$nom,$prenom,$mdp);
    mysqli_stmt_execute($stmt);
}


//Inserer un nouveau groupe de tp
function insertgrptp($session,$nomtp,$numtd){
    $stmt = mysqli_prepare($session, 'INSERT INTO groupestp(numTD,nomTP) VALUES(?,?)');
    mysqli_stmt_bind_param($stmt,"is",$numtd,$nomtp);
    mysqli_stmt_execute($stmt);
    
}

//Inserer un nouveau groupe de td
function insertgrptd($session,$nomtd,$nums){
    $stmt = mysqli_prepare($session, 'INSERT INTO groupestd(numSec,nomTD) VALUES(?,?)');
    mysqli_stmt_bind_param($stmt,"is",$nums,$nomtd);
    mysqli_stmt_execute($stmt);
    
}

//Inserer une nouvelle section
function insertsection($session,$nomsec,$numsem){
    $stmt = mysqli_prepare($session, 'INSERT INTO sections(nomSec,numSem) VALUES(?,?)');
    mysqli_stmt_bind_param($stmt,"si",$nomsec,$numsem);
    mysqli_stmt_execute($stmt);
    
}

// MàJ de Sections
function updatesec($session,$nsec,$newnomsec,$newnumsem){
    $stmt = mysqli_prepare($session, 'UPDATE sections SET nomSec=?, numSem=? WHERE numSec=?');
    mysqli_stmt_bind_param($stmt,"sii",$newnomsec,$newnumsem,$nsec);
    mysqli_stmt_execute($stmt);
}

// MàJ de TD
function updatetd($session,$ntd,$newsec,$newnomtd){
    $stmt = mysqli_prepare($session, 'UPDATE groupestd SET nomTD=?, numSec=?  WHERE numTD=?');
    mysqli_stmt_bind_param($stmt,"sii",$newnomtd,$newsec,$ntd);
    mysqli_stmt_execute($stmt);
}

// MàJ de TP
function updatetp($session,$ntp,$newtd,$newnomtp){
    $stmt = mysqli_prepare($session, 'UPDATE groupestp SET nomTP=?, numTD=?  WHERE numTP=?');
    mysqli_stmt_bind_param($stmt,"sii",$newnomtp,$newtd,$ntp);
    mysqli_stmt_execute($stmt);
}

// MàJ d'Etudiants
function updateetu($session,$netu,$newmdp,$newtp){
    $mdp=password_hash($newmdp,PASSWORD_DEFAULT);

    $stmt = mysqli_prepare($session, 'UPDATE etudiants SET mdp=?, numTP=?  WHERE numEtu=?');
    mysqli_stmt_bind_param($stmt,"sii",$mdp,$newtp,$netu);
    mysqli_stmt_execute($stmt);
}

// MàJ d'Enseignants
function updateens($session,$nens,$newmdp){
    $mdp=password_hash($newmdp,PASSWORD_DEFAULT);

    $stmt = mysqli_prepare($session, 'UPDATE enseignants SET mdp=? WHERE numEns=?');
    mysqli_stmt_bind_param($stmt,"si",$mdp,$nens);
    mysqli_stmt_execute($stmt);
}


function updatemdpens($session,$nens,$newmdp){
    $mdp=password_hash($newmdp,PASSWORD_DEFAULT);

    $stmt = mysqli_prepare($session, 'UPDATE enseignants SET mdp=? WHERE numEns=?');
    mysqli_stmt_bind_param($stmt,"si",$mdp,$nens);
    mysqli_stmt_execute($stmt);
}

function updatemdpetu($session,$netu,$newmdp){
    $mdp=password_hash($newmdp,PASSWORD_DEFAULT);

    $stmt = mysqli_prepare($session, 'UPDATE etudiants SET mdp=? WHERE numEtu=?');
    mysqli_stmt_bind_param($stmt,"si",$mdp,$netu);
    mysqli_stmt_execute($stmt);
}

// MàJ d'Experiences
function updateexp($session,$nexp,$newtitre){

    $stmt = mysqli_prepare($session, 'UPDATE experiences SET titre=? WHERE numExp=?');
    mysqli_stmt_bind_param($stmt,"si",$newtitre,$nexp);
    mysqli_stmt_execute($stmt);
}

// ajouter une colonne mère à une colonne
function updatecolonnefille($session,$numcolfille,$numcolmere){
    $stmt = mysqli_prepare($session, 'UPDATE colonnes SET numColMere=? WHERE numCol=?');
    mysqli_stmt_bind_param($stmt,"ii",$numcolmere,$numcolfille);
    mysqli_stmt_execute($stmt);
}

//Recupere le titre d'une experience selon son numéro
 function recuptitreexperience($session,$ne){
    $stmt = mysqli_prepare($session, "SELECT titre FROM experiences WHERE numExp=".$ne."");
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $titreExp);
    mysqli_stmt_fetch($stmt);
    return $titreExp;
}

//Recupere le titre d'une experience selon son numéro
 function recupresumeexp($session,$ne){
    $stmt = mysqli_prepare($session, "SELECT resume FROM experiences WHERE numExp=".$ne."");
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $resu);
    mysqli_stmt_fetch($stmt);
    return $resu;
}

//Recupere le nom d'une section selon son numéro
function recupnomsec($session,$ns){
    $stmt = mysqli_prepare($session, "SELECT nomSec FROM sections WHERE numSec=".$ns."");
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $nomsection);
    mysqli_stmt_fetch($stmt);
    return $nomsection;
}

function recupsemestredeletudiant($session,$numetu){
    $stmt = mysqli_prepare($session, "SELECT numSem FROM sections s, etudiants e, groupestd td, groupestp tp WHERE e.numTP=tp.numTP and tp.numTD=td.numTD and td.numSec=s.numSec and numEtu=".$numetu."");
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $numsem);
    mysqli_stmt_fetch($stmt);
    return $numsem;
}

//Recupere le nom d'un TD selon son numéro
function recupnomTD($session,$ntd){
     $stmt = mysqli_prepare($session, "SELECT nomTD FROM groupestd WHERE numTD=".$ntd."");
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $nomtd);
    mysqli_stmt_fetch($stmt);
    return $nomtd;
}

//Recupere le nom d'un TP selon son numéro
function recupnomTP($session,$ntp){
     $stmt = mysqli_prepare($session, "SELECT nomTP FROM groupestp WHERE numTP=".$ntp."");
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $nomtp);
    mysqli_stmt_fetch($stmt);
    return $nomtp;
}

//Recupere l'année de la dateFin d'une experience
function recupanneefinExp ($session,$ne){
    $stmt = mysqli_prepare($session, "SELECT YEAR(dateFin) FROM experiences WHERE numExp=".$ne."");
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $datef);
    mysqli_stmt_fetch($stmt);
    return $datef;
}

//Recupere toutes les colonnes filles d'une experience
function recupcolonnesfilles($session,$ne){
    $query = 'SELECT * FROM colonnes WHERE numExp='.$ne.' and numColMere is not NULL';
    $result = mysqli_query ($session, $query); 
    return $result;
}

//Recupere toutes les colonnes filles d'une experience
function recupcolonnesmere($session,$ne){
    $query = 'SELECT * FROM colonnes WHERE numExp='.$ne.' and numColMere is NULL';
    $result = mysqli_query ($session, $query); 
    return $result;
}

//Recupere toutes les colonnes mères d'une experience
function recupmeredunefille($session,$nc,$ne){
    //$query = "SELECT * FROM colonnes WHERE numCol= ? and numExp=1";
    $stmt=mysqli_prepare($session,'SELECT libelle FROM colonnes WHERE numCol= ? and numExp=?'); 
    mysqli_stmt_bind_param($stmt,'ii',$nc,$ne);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $numutactuel);
    mysqli_stmt_fetch($stmt);
    return $numutactuel;
}

//Recupere toutes les données d'une colonne pour une experience
function recupdonneesall($session,$nc,$ne){
    $query = 'SELECT * FROM remplit r, colonnes c,etudiants e WHERE c.numCol=r.numCol and r.numEtu=e.numEtu and r.numCol='.$nc.' and c.numExp='.$ne.'';
    $result = mysqli_query ($session, $query); 
    return $result;
}

//Recupere toutes les données d'une colonne pour une experience pour une section données
function recupdonneesexpS($session,$nc,$ne,$ns){
  $query = 'SELECT * FROM remplit r, colonnes c,etudiants e, groupestp gtp, groupestd gtd, sections s  WHERE c.numCol=r.numCol and r.numEtu=e.numEtu and e.numTP=gtp.numTP and gtp.numTD=gtd.numTD and gtd.numSec=s.numSec and s.numSec='.$ns.' and r.numCol='.$nc.' and c.numExp='.$ne.'';
    $result = mysqli_query ($session, $query); 
    return $result;
}

//Recupere toutes les données d'une colonne pour une experience pour un groupe de td données
function recupdonneesexpTD($session,$nc,$ne,$ns,$ntd){
    $query = 'SELECT * FROM remplit r, colonnes c,etudiants e, groupestp gtp, groupestd gtd, sections s  WHERE c.numCol=r.numCol and r.numEtu=e.numEtu and e.numTP=gtp.numTP and gtp.numTD=gtd.numTD and gtd.numSec=s.numSec and s.numSec='.$ns.' and gtd.numTD='.$ntd.' and r.numCol='.$nc.' and c.numExp='.$ne.'';
    $result = mysqli_query ($session, $query); 
    return $result;
}

//Recupere toutes les données d'une colonne pour une experience pour un groupe de tp données
function recupdonneesexpTP($session,$nc,$ne,$ns,$ntd,$ntp){
 $query = 'SELECT * FROM remplit r, colonnes c,etudiants e, groupestp gtp, groupestd gtd, sections s  WHERE c.numCol=r.numCol and r.numEtu=e.numEtu and e.numTP=gtp.numTP and gtp.numTD=gtd.numTD and gtd.numSec=s.numSec and s.numSec='.$ns.' and gtd.numTD='.$ntd.' and gtp.numTP='.$ntp.' and r.numCol='.$nc.' and c.numExp='.$ne.'';
    $result = mysqli_query ($session, $query); 
    return $result;
}


// Genere le fichier excel faisant office de rapport
function genererexcel($session,$ns,$ntd,$ntp,$nexp){
    $alpha = array(
            '1' => 'A',
            '2' => 'B',
            '3' => 'C',
            '4' => 'D',
            '5' => 'E',
            '6' => 'F',
            '7' => 'G',
            '8' => 'H',
            '9' => 'I',
            '10' => 'J',
            '11' => 'K',
            '12' => 'L',
            '13' => 'M',
            '14' => 'N',
            '15' => 'O',
            '16' => 'P',
            '17' => 'Q',
            '18' => 'R',
            '19' => 'S',
            '20' => 'T',
            '21' => 'U',
            '22' => 'V',
            '23' => 'W',
            '24' => 'X',
            '25' => 'Y',
            '26' => 'Z',
         );
        //Crée un nouveau fichier
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValueByColumnAndRow(1,1,"N° Etudiant");
        $titreExp=recuptitreexperience($session,$nexp); //Recupere le titre d'une experience
        $noms=recupnomsec($session,$ns); //Recupere le nom d'une section
        $nomtd=recupnomTD($session,$ntd); //Recupere le nom d'un groupe de TD
        $nomtp=recupnomTP($session,$ntp); //Recupere le nom d'un groupe de TP
        $nbmere=combiendecolmerepouruneexp($session,$nexp); //Recupere le nombre de colonne 'mere' pour une experience
        $dateexp=recupanneefinExp($session,$nexp); //Recupere l'année de fin d'une experience
        $resultfille=recupcolonnesfilles($session,$nexp); //Recupere toutes les colonnes filles d'une experience
        $resultmere=recupcolonnesmere($session,$nexp);
        $i=0;
        $j=0;

        while ($ligne=mysqli_fetch_array ($resultfille)) { //Pour chaque colonne fille :
            $mere=recupmeredunefille($session,$ligne["numColMere"],$nexp); //Recupere la colonne mere
            $sheet->setCellValueByColumnAndRow(2+$i,1,$mere); //Remplit la cellule (colonne 2+i , ligne 1) du libelle de la colonne mere
            $sheet->setCellValueByColumnAndRow(2+$i,2,$ligne["libelle"]); //Remplit la cellule d'en dessous du libelle de la colonne fille
            if($ns>-2){ //Si on genere l'experience après sa création 
                if($ns>-1){ //Si l'utilisateur à selectionne une section
                    if($ntd>-1){ //Si l'utilisateur à selectionne un groupe de TD
                        if($ntp>-1){ //Si l'utilisateur à selectionne un groupe de TP
                            $d=recupdonneesexpTP($session,$ligne["numCol"],$nexp,$ns,$ntd,$ntp); //Recupere les données remplit par les etudiants de ce TP pour cette colonne
                            $titre=$titreExp."-".$dateexp."-".$noms."-".$nomtd."-".$nomtp; // Le titre du document xlsx sera : Titre de l'experience-annee de fin de l'experience-nom de la section choisie-nom du groupe de td choisi-nom du groupe de tp
                        }
                        else{ //sinon
                            $d=recupdonneesexpTD($session,$ligne["numCol"],$nexp,$ns,$ntd);//Recupere les données remplit par les etudiants de ce TD pour cette colonne
                            $titre=$titreExp."-".$dateexp."-".$noms."-".$nomtd; // Le titre du document xlsx sera : Titre de l'experience-annee de fin de l'experience-nom de la section choisie-nom du groupe de td choisi
                        }

                    }
                    else{//sinon
                        $d=recupdonneesexpS($session,$ligne["numCol"],$nexp,$ns); //Recupere les données remplit par les etudiants de cette section pour cette colonne
                        $titre=$titreExp."-".$dateexp."-".$noms;// Le titre du document xlsx sera : Titre de l'experience-annee de fin de l'experience-nom de la section choisie
                    }
                }
                else{//sinon
                        $d=recupdonneesall($session,$ligne["numCol"],$nexp);//Recupere toutes les données remplit par les etudiants pour cette colonne
                        $titre=$titreExp."-".$dateexp."-All"; // Le titre du document xlsx sera : Titre de l'experience-annee de fin de l'experience-All

                }
                $j=0;
                while($l=mysqli_fetch_array($d)){ //Pour chaque donnée recuperee
                    $sheet->setCellValueByColumnAndRow(2+$i,3+$j,$l["donnees"]); //Remplit la cellule (colonne 2+i , ligne 3+j) de la donnee
                    $sheet->setCellValueByColumnAndRow(1,3+$j,$l["numEtu"]); // Remplit sur la meme ligne en premiere colonne le numero etudiant associé
                    $j+=1; //incremente j
                }
            }
            else{ //Sinon
                $titre=$titreExp."-".$dateexp;// Le titre du document xlsx sera : Titre de l'experience-annee de fin de l'experience
            }
            $i+=1; //incremente i
        }

        //Définit les bordures et la couleur du background des cellules
        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFA0A0A0',
                ],
            ],
        ];
        
        //Permet la fusion de cellules
        $spreadsheet->getActiveSheet()->mergeCells($alpha[1]."1:A2");
        $mere=recupcolonnesmere($session,$nexp);
        $j=2;
        $meressfille=0;
        while ($ligne=mysqli_fetch_array ($mere)) {
            $nbfille=combiendesouscolonnepourunemere($session,$ligne["numCol"]);
            if($nbfille>0){
                $spreadsheet->getActiveSheet()->mergeCells($alpha[$j]."1:".$alpha[$nbfille+$j-1]."1");
            }
            else{
                $sheet->setCellValueByColumnAndRow(2+$i,1,$ligne["libelle"]);
                $meressfille+=1;
                $i+=1;
            }
            $j=$j+$nbfille;
        }  
        $sheet->getStyle('A1:'.$alpha[$j+$meressfille-1].'2')->applyFromArray($styleArray);




        //Permet de centrer verticalement et horizontalement le contenu des cellules
        $hor_center = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER;
        $ver_center = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;
        foreach($sheet->getRowIterator() as $row) {
            foreach($row->getCellIterator() as $cell) {
                $cellCoordinate = $cell->getCoordinate();
                $sheet->getStyle($cellCoordinate)->getAlignment()->setHorizontal($hor_center);
                $sheet->getStyle($cellCoordinate)->getAlignment()->setVertical($ver_center);
            }
        }

        //Enregistre le fichier avec le titre crée precedement
if($ns=-2){
        $titre=$titreExp."-".$dateexp;}
        $titre=$titre.".xlsx";
        $writer = new Xlsx($spreadsheet);
        $writer->save("./RapportsExcel/".$titre); 
        afficheexcel($session,$titre);
        if($ns>-2){ //Si on genere l'experience après sa création 
            //Affiche le bouton de téléchargement du fichier
            echo "Le fichier excel de l'experience : ".$titreExp." a été généré avec succés !";
            echo"</br> <form> <input class=";
            echo '"favorite styled" type="button" value="Télécharger le fichier" onclick="window.location=';
            echo"'./RapportsExcel/".$titre."';";
            echo '">';
        }
    } 

//Affiche une preview d'une experience
    function afficheexcel($session,$nom){

       if("./RapportsExcel/".$nom != '')
{
    $allowed_extension = array('xls', 'xlsx');
    $file_array = explode(".", "./RapportsExcel/".$nom);
    $file_extension = end($file_array);
    if(in_array($file_extension, $allowed_extension))
    {
        $reader = IOFactory::createReader('Xlsx');
        $spreadsheet = $reader->load("./RapportsExcel/".$nom);
        $writer = IOFactory::createWriter($spreadsheet, 'Html');
        $message = $writer->save('php://output');
    }
    else
    {
        $message = '<div class="alert alert-danger">Only .xls or .xlsx file allowed</div>';
    }
}
else
{
    $message = '<div class="alert alert-danger">Please Select File</div>';
}

echo $message;
  }

// Affiche une preview et le formulaire de l'experience à remplir
    function afficheexcelremplir($session,$num){
        $nom=recuptitreexperience($session,$num).".xlsx";

       if("./RapportsExcel/".$nom != '')
{
    $allowed_extension = array('xls', 'xlsx');
    $file_array = explode(".", "./RapportsExcel/".$nom);
    $file_extension = end($file_array);
    if(in_array($file_extension, $allowed_extension))
    {
        $reader = IOFactory::createReader('Xlsx');
        $spreadsheet = $reader->load("./RapportsExcel/".$nom);
        $writer = IOFactory::createWriter($spreadsheet, 'Html');
        $message = $writer->save('php://output');
        echo "<input type=text />";
    }
    else
    {
        $message = '<div class="alert alert-danger">Only .xls or .xlsx file allowed</div>';
    }
}
else
{
    $message = '<div class="alert alert-danger">Please Select File</div>';
}

echo $message;
  }

//Affiche le bouton permettant de telecharger le fichier xlsx d'un experience
function telechargeexcel($session,$ns,$ntd,$ntp,$nexp){
    $titreExp=recuptitreexperience($session,$nexp);
    $noms=recupnomsec($session,$ns);
    $nomtd=recupnomTD($session,$ntd);
    $nomtp=recupnomTP($session,$ntp);
        $dateexp=recupanneefinExp($session,$nexp);
    
    if($ns>-1){
        if($ntd>-1){
            if($ntp>-1){
                $titre=$titreExp."-".$dateexp."-".$noms."-".$nomtd."-".$nomtp.".xlsx";
            }
            else{
                $titre=$titreExp."-".$dateexp."-".$noms."-".$nomtd.".xlsx";
            }
        }
        else{
            $titre=$titreExp."-".$dateexp."-".$noms.".xlsx";
        }
    }
    else{
        $titre=$titreExp."-".$dateexp."-All".".xlsx";
    }
            //echo "Le fichier excel de l'experience : ".$titreExp." a été généré avec succés !";
            echo"</br> <form> <input class=";
            echo '"favorite styled" type="button" value="Télécharger le fichier" onclick="window.location=';
            echo"'./RapportsExcel/".$titre."';";
            echo '">';
        }
?>
