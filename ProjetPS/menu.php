<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Menu</title>
	<meta name="author" content="Sara Soueidan for Codrops" />
	<link rel="shortcut icon" href="../favicon.ico">
	<link rel="stylesheet" type="text/css" href="css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="css/demo.css" />
	<link rel="stylesheet" type="text/css" href="css/component2" />
	<script src="js/modernizr-2.6.2.min.js"></script>

	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-7243260-2']);
		_gaq.push(['_trackPageview']);
		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>


</head>
<?php
require 'fonctions.php';
session_start();
$session=connexionbd();
 if (isset($_SESSION['typeuser'])){
 	if ($_SESSION['typeuser'] == "etu"){
            $_SESSION['numsem']= recupsemestredeletudiant($session,$_SESSION['login']);
 		
 		echo '<body>
	<div class="container">
		<!-- Top Navigation -->			<header>
			<h1>Plateforme gestion de données <span>licence Sciences de la Vie</span></h1>
			
		</header>
		<div class="component">
			
			<!-- Start Nav Structure -->
			<button class="cn-button" id="cn-button">Menu</button>
			<div class="cn-wrapper" id="cn-wrapper">
				<ul>
					<li><a href="moncompteetu.php"><span>Mon compte</span></a></li>
					<li><a href="choixexpremplir.php"><span>Remplir Exp</span></a></li>
					<li><a href="choixgenerer.php"><span>Rapport</span></a></li>
				</ul>
			</div>
			<!-- End of Nav Structure -->
		</div>

	</div><!-- /container -->
			<button class="cn-button" id="cn-button" ><a href="deco.php">Logout</a></button>

	<script src="js/polyfills.js"></script>
	<script src="js/demo2.js"></script>
	<!-- For the demo ad only -->
	<script src="http://tympanus.net/codrops/adpacks/demoad.js"></script>
</body>
</html>';
 	}
 	elseif ($_SESSION['typeuser'] == "ens"){
 		echo'<body>
	<div class="container">
		<!-- Top Navigation -->			<header>
			<h1>Plateforme gestion de données <span>licence Sciences de la Vie</span></h1>
			
		</header>
		<div class="component">
			<!-- Start Nav Structure -->
			<button class="cn-button" id="cn-button">Menu</button>
			<div class="cn-wrapper" id="cn-wrapper">
				<ul>
					<li><a href="moncompteens.php"><span>Mon compte</span></a></li>
					<li><a href="essai.php"><span>New Exp</span></a></li>
					<li><a href="choixgenerer.php"><span>Rapport</span></a></li>
				</ul>
			</div>
			<!-- End of Nav Structure -->
		</div>

	</div><!-- /container -->
			<button class="cn-button" id="cn-button" ><a href="deco.php">Logout</a></button>

	<script src="js/polyfills.js"></script>
	<script src="js/demo2.js"></script>
	<!-- For the demo ad only -->
	<script src="http://tympanus.net/codrops/adpacks/demoad.js"></script>
</body>
</html>';
 	}
 	elseif ($_SESSION['typeuser'] == "admin"){
 		echo'<body>
	<div class="container">
		<!-- Top Navigation -->			<header>
			<h1>Plateforme gestion de données <span>licence Sciences de la Vie</span></h1>
			
			
		</header>
		<div class="component">
			<!-- Start Nav Structure -->

			<button class="cn-button" id="cn-button">Menu</button>

			<div class="cn-wrapper" id="cn-wrapper">
				<ul>
					<li><a href="adminformetu.php"><span>Etudiants</span></a></li>
					<li><a href="adminformens.php"><span>Enseignants</span></a></li>
					<li><a href="adminformsection.php"><span>Sections</span></a></li>
					<li><a href="adminformtd.php"><span>TD</span></a></li>
					<li><a href="adminformtp.php"><span>TP</span></a></li>
					<li><a href="adminformexp.php"><span>Exp</span></a></li>
				</ul>
			</div>


			<!-- End of Nav Structure -->
			<div>

			</div>

		</div>

	</div>
			<button class="cn-button" id="cn-button" ><a href="deco.php">Logout</a></button>

	<!-- /container -->
	<script src="js/polyfills.js"></script>
	<script src="js/demo2.js"></script>
	<!-- For the demo ad only -->
	<script src="http://tympanus.net/codrops/adpacks/demoad.js"></script>
</body>
</html>
';
 	}
 }
 else{
    	header("location:index.php");
 }
