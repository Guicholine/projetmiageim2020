<?php
	require 'fonctions.php';
	$session=connexionbd();

	if(isset($_POST['ID'])){
		$user_login = $_POST["ID"];	
		if(is_unique_login($session,$user_login)){
			$user_password = $_POST["MDP"];
	        $user_password = password_hash($_POST["MDP"],PASSWORD_DEFAULT);
			insertetudiant($session,$user_login,$user_password,$_POST["numTP"],$_POST["nom"],$_POST['prenom']);
		}
	}
	else{
	header("location:menu.php");
}

	function is_unique_login($session,$user_login){
		$stmt = mysqli_prepare($session, "SELECT numEtu from etudiants where numEtu = ?");
		mysqli_stmt_bind_param($stmt, "s", $user_login);
		mysqli_stmt_execute($stmt);
		if(mysqli_stmt_fetch($stmt)==TRUE){
			return False;
		}
		else{
			return True;
		}
	}
?>