<?php
require 'fonctions.php';
session_start();
  
  $session=connexionbd();
  if (!(isset($_SESSION['typeuser']) and isset($_SESSION["login"]) and $_SESSION['typeuser']=="admin" and $_SESSION['login']=="admin")) {
  header("location:index.php"); 
}
else {
?>


<!DOCTYPE HTML>

<html>
  <head>
	<title> Ajouter un étudiant</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
   <script type="text/javascript">
    function myFunction() {
  var x = document.getElementById("MDP");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
} </script>
  </head>


  <body>
    <h3><center>Ajouter un nouvel étudiant</center></h3>
    </br>
    </br>
    <div class="col-md-9">
      <div class="container">
        <div class="row">
          <fieldset style="width: 500px">
            <form method="POST" action="testinscriptionetu.php" onsubmit="if(!confirm('Confirmez-vous cette action ?')){
          return false;}">
			    <input type="text" name="ID" placeholder="Identifiant" required="true" />
          <div><input type="text" name="nom" placeholder="Nom de l'étudiant" required="true" /></div>
          <div><input type="text" name="prenom" placeholder="Prénom de l'étudiant" required="true" /></div>

			    
          <div><input type="password" name="MDP" id="MDP" placeholder="Mot de passe" required="true"/>
      <input type="checkbox" onclick="myFunction()" >Show Password </div>

			    <label>Séléctionnez le TP dans lequel l'étudiant est inscrit</label>
                  <select name='numTP' id='section'>
                    
                     <?php
                      $res = mysqli_query($session,"SELECT * FROM groupestp gtp, groupestd gtd,sections s where s.numSec=gtd.numSec and gtp.numTD=gtd.numTD");
      while($row = mysqli_fetch_assoc($res)){
        echo "<option value='".$row["numTP"]."'>".$row["numSem"]."-".$row["nomSec"]."-".$row["nomTD"]."-".$row["nomTP"]."</option>";
      }
      ?>
                  
                  </select>
			    <button type="submit" name="t" >Inscrire</button>
			</form>
          </fieldset>
        </div>
      </div>
    </div>
  </body>
</html>
<?php
}
?>
