<?php
require 'fonctions.php';
session_start();

$session=connexionbd();
if (!(isset($_SESSION['typeuser']) and isset($_SESSION["login"]))) {
  header("location:index.php");
}
else {
 ?>
 <html>
 <head>
    <title>Rapport experience</title>
    <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="css/form">

    <script type='text/javascript'>

        function getXhr(){
            var xhr = null;
                if(window.XMLHttpRequest) // Firefox et autres
                   xhr = new XMLHttpRequest();
                else if(window.ActiveXObject){ // Internet Explorer
                   try {
                    xhr = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e) {
                    xhr = new ActiveXObject("Microsoft.XMLHTTP");
                }
            }
                else { // XMLHttpRequest non supporté par le navigateur
                   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
                   xhr = false;
               }
               return xhr;
           }

            /**
            * Méthode qui sera appelée sur le clic du bouton
            */
            function go(){
                var xhr = getXhr();
                // On définit ce qu'on va faire quand on aura la réponse
                xhr.onreadystatechange = function(){
                    // On ne fait quelque chose que si on a tout reçu et que le serveur est OK
                    if(xhr.readyState == 4 && xhr.status == 200){
                        leselect = xhr.responseText;
                        // On se sert de innerHTML pour rajouter les options à la liste
                        document.getElementById('td').innerHTML = leselect;
                    }
                }

                // Ici on va voir comment faire du post
                xhr.open("POST","ajax.php",true);
                // ne pas oublier ça pour le post
                xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
                // ne pas oublier de poster les arguments
                // ici, l'id de l'auteur
                sel = document.getElementById('section');
                idsection = sel.options[sel.selectedIndex].value;
                xhr.send("idSection="+idsection);
            }





            function gotp(){
                var xhr = getXhr();
                // On définit ce qu'on va faire quand on aura la réponse
                xhr.onreadystatechange = function(){
                    // On ne fait quelque chose que si on a tout reçu et que le serveur est OK
                    if(xhr.readyState == 4 && xhr.status == 200){
                        leselect = xhr.responseText;
                        // On se sert de innerHTML pour rajouter les options à la liste
                        document.getElementById('tp').innerHTML = leselect;
                    }
                }

                // Ici on va voir comment faire du post
                xhr.open("POST","ajax2.php",true);
                // ne pas oublier ça pour le post
                xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
                // ne pas oublier de poster les arguments
                // ici, l'id de l'auteur
                sel = document.getElementById('td');
                idsection = sel.options[sel.selectedIndex].value;
                xhr.send("idTd="+idsection);
            }

        </script>
    </head>

    <body>
        <div class="col-md-9">
          <div class="container">
            <div class="row">
              <fieldset style="width: 500px">
                <form action="generer.php" method="POST">
                 <?php
                 if($_SESSION['typeuser']=='ens'){
                    echo "<h3><center>Generer le rapport d'une experience</center></h3>";
                }
                else{
                    echo "<h3><center>Télécharger le rapport d'une experience</center></h3>";

                }
                ?>
                <label>Section</label>
                <select name='section' id='section' onchange='go()'>
                    <option value='-1'>Toutes les sections</option>
                    <?php
                    $res = mysqli_query($session,"SELECT * FROM sections");
                    while($row = mysqli_fetch_assoc($res)){
                        echo "<option value='".$row["numSec"]."'>".$row["numSem"]."-".$row["nomSec"]."</option>";
                    }

                    ?>

                </select><br>
                <label>TD</label>
                <div  style='display:inline' onchange='gotp()'>
                    <select name='td' id='td'>
                        <option value='-1'>Tous les TD</option>
                    </select>
                </div> <br>
                <label>TP</label>
                <div id='tp' style='display:inline' >
                    <select name='tp' id='tp'>
                        <option value='-1'>Tous les TP</option>
                    </select>
                </div>
                <div>
                    <label>Selectionnez une experience</label>
                    <select name='experience' id='experience'>

                        <?php
                        $res = mysqli_query($session,"SELECT numExp,titre,YEAR(dateFin) as dtf,numSem FROM experiences");
                        while($row = mysqli_fetch_assoc($res)){
                            echo "<option value='".$row["numExp"]."'>".$row["numSem"]."-".$row["titre"]."-".$row["dtf"]."</option>";
                        }
                        ?>
                    </select>
                </div>
                <div>
                    <?php
                    if($_SESSION['typeuser']=='ens'){
                        echo '<p><button type="submit" value="connecter">Générer</button></p>';
                    }
                    else{
                        echo '<p><button type="submit" value="connecter">Poursuivre</button></p>';
                    }
                    ?>

                </div>
                <!-- <label>Livres</label>
                <div id='livre' style='display:inline'>
                <select name='livre'>
                    <option value='-1'>Choisir un auteur</option>
                </select>
            </div> -->
        </form>
    </fieldset>
</div>

</div>
</div>
</body>
</html>
<?php
}
?>
