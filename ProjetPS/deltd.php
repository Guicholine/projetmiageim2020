<?php
require 'fonctions.php';
session_start();
  
  $session=connexionbd();
  if (!(isset($_SESSION['typeuser']) and isset($_SESSION["login"]) and $_SESSION['typeuser']=="admin" and $_SESSION['login']=="admin")) {
  header("location:index.php"); 
}
else {
?>

  <!DOCTYPE HTML>

  <html>
    <head>
      <title> Supprimer un TD </title>
      <meta charset="utf-8">
      <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
    </head>


    <body>
          <h3><center>Supprimer un TD</center></h3>
          </br>
          </br>
      <div class="col-md-9">
        <div class="container">
          <div class="row">
            <fieldset style="width: 500px">
              <form method="POST" action='supprtd.php' onsubmit="if(!confirm('Attention ! La suppression est en cascade et irréversible en cas de doutes veuillez consulter le schéma de la BD')){
            return false;}">
                <div class="row">
                  <div class="col-md-12">
                    <label>Séléctionnez le TD à supprimer</label>
                    <select name='numTD' id='section'>
                      <?php
                        $res = mysqli_query($session,"SELECT * FROM groupestd gtd,sections s where s.numSec=gtd.numSec");
        while($row = mysqli_fetch_assoc($res)){
          echo "<option value='".$row["numTD"]."'>".$row["numSem"]."-".$row["nomSec"]."-".$row["nomTD"]."</option>";
        }
                      ?>
                    </select>
                  </div>
                  <div class="col-md-12">
                    <input type="submit" value="Supprimer le TD" name="submit">
                  </div>
                </div>
              </form>
            </fieldset>
          </div>
        </div>
      </div>
    </body>
  </html>
<?php
}
?>