<?php
require 'fonctions.php';
session_start();
  
  $session=connexionbd();
  if (!(isset($_SESSION['typeuser']) and isset($_SESSION["login"]) and $_SESSION['typeuser']=="admin" and $_SESSION['login']=="admin")) {
  header("location:index.php"); 
}
else {
?>


<!DOCTYPE HTML>

<html>
  <head>
    <title> Modifier un TP </title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
  </head>


  <body>
        <h3><center>Modifier un TP</center></h3>
        </br>
        </br>
    <div class="col-md-9">
      <div class="container">
        <div class="row">
          <fieldset style="width: 500px">
            <form method="POST" action='majtp.php' onsubmit="if(!confirm('Confirmer la modification')){
          return false;}">
              <div class="row">
                <div class="col-md-12">
                                    <label>Séléctionnez le TP à modifier</label>
                  <select name='numTP' id='section'>
                    <?php
                      $res = mysqli_query($session,"SELECT * FROM groupestp gtp, groupestd gtd,sections s where s.numSec=gtd.numSec and gtp.numTD=gtd.numTD");
                      while($row = mysqli_fetch_assoc($res)){
                        echo "<option value='".$row["numTP"]."'>".$row["numSem"]."-".$row["nomSec"]."-".$row["nomTD"]."-".$row["nomTP"]."</option>";
                      }
                    ?>
                  </select>
                  <label>Séléctionnez le nouveau TD associé</label>
                  <select name='numTD' id='section'>
                    <?php
                      $session=connexionbd();
                      $res = mysqli_query($session,"SELECT * FROM groupestd gtd,sections s where s.numSec=gtd.numSec");
      while($row = mysqli_fetch_assoc($res)){
        echo "<option value='".$row["numTD"]."'>".$row["numSem"]."-".$row["nomSec"]."-".$row["nomTD"]."</option>";
      }
                    ?>
                  </select>
                </div>
                <div class="col-md-12">
                  <label>Entrez le nouveau nom de TP</label>
                  <input type="text" name="newnomtp" required="true">
                </div>
                <div class="col-md-12">
                  <input type="submit" value="Modifier le TP" name="submit">
                </div>
              </div>
            </form>
          </fieldset>
        </div>
      </div>
    </div>
  </body>
</html>
<?php
}
?>