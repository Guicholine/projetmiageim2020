<?php
require 'fonctions.php';
session_start();
$session=connexionbd();

?>


<!DOCTYPE HTML>

<html>
  <head>
	<title> Ajouter une section</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="css/form">
  </head>


  <body>



      <div class="container">
        <div class="row">
          <fieldset style="width: 500px">
            <form method="POST" action='formRemplit.php'>
              <h3><center>Selectionnez l'experience à remplir</center></h3>
              <select name='experience' id='experience'>
                    <?php
                        $res = mysqli_query($session,"SELECT * FROM experiences where numSem=".$_SESSION["numsem"]."");
                        while($row = mysqli_fetch_assoc($res)){
                            echo "<option  value='".$row["numExp"]."'>".$row["titre"]."</option>";
                        }
                    ?>
                </select>
              <div class="row">
                <div class="col-md-12">
                  <input type="submit" value="Poursuivre" name="submit">
                </div>
              </div>
            </form>
          </fieldset>
        </div>
      </div>

  </body>
</html>

