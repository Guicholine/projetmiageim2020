<?php
require 'fonctions.php';
session_start();
  
  $session=connexionbd();
  if (!(isset($_SESSION['typeuser']) and isset($_SESSION["login"]) and $_SESSION['typeuser']=="admin" and $_SESSION['login']=="admin")) {
  header("location:index.php"); 
}
else {
?>

  <!DOCTYPE HTML>

  <html>
    <head>
      <title> Ajouter une section</title>
      <meta charset="utf-8">
      <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
    </head>


    <body>
      <h3><center>Ajouter une nouvelle section</center></h3>
      </br>
      </br>
      <div class="col-md-9">
        <div class="container">
          <div class="row">
            <fieldset style="width: 500px">
              <form method="POST" action='addSection.php' onsubmit="if(!confirm('Confirmez-vous cette action ?')){
            return false;}">
                <label for="nomSec"> Veuillez donner un nom à la section : </label>
                <input type="text" name ="NomSec" required="true">
                <div class="row">
                  <div class="col-md-12">
                  <label>Séléctionnez le semestre correspondant</label>
                  <select name='numSem' id='semestre'>
                    <?php
                      $res = mysqli_query($session,"SELECT * FROM semestres");
                      while($row = mysqli_fetch_assoc($res)){
                        echo "<option value='".$row["numSem"]."'>".$row["numSem"]."</option>";
                      }
                    ?>
                  </select>
                </div>
                  <div class="col-md-12">
                    <input type="submit" value="Créer la nouvelle section" name="submit">
                  </div>
                </div>
              </form>
            </fieldset>
          </div>
        </div>
      </div>
    </body>
  </html>

<?php
}
?>