<?php
require 'fonctions.php';
session_start();
$session=connexionbd();

if (!(isset($_SESSION['typeuser']) and isset($_SESSION["login"]) and $_SESSION['typeuser']!="etu")) {
  header("location:index.php");
}
else {}
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Nouvelle expérience</title>
        <link rel="stylesheet" href="css/form">
    </head>
    <body>
        <div class="container">
          <form method="POST" action="fromExp.php">
            <h3>Selectionnez une experience</h3>
            <select name='experience' id='experience'>

                <?php
                $res = mysqli_query($session,"SELECT numExp,titre,YEAR(dateFin) as dtf,numSem FROM experiences");
                while($row = mysqli_fetch_assoc($res)){
                    echo "<option value='".$row["numExp"]."'>".$row["numSem"]."-".$row["titre"]."-".$row["dtf"]."</option>";
                }
                ?>
            </select>
            <input type="submit" value="Load">
        </form>
    </div>

</body>
</html>




