<?php
require 'fonctions.php';
session_start();
$session=connexionbd();
if (isset($_SESSION['typeuser']) and isset($_SESSION["login"])) {
	header("location:menu.php"); 
}
else {
	if (isset($_POST["type"])){
		?>
	<!DOCTYPE html>
	<html>
	<head>
	 <link rel="stylesheet" media="screen" type="text/css" href="css/loginStyle.css"/>
	 <meta charset="utf-8" />
	 <script type="text/javascript">
	 	function myFunction() {
  var x = document.getElementById("MDP");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
} </script>
	 <title>UT3 - Authentification </title>
	</head>
	<?php 
	echo'
<body>

	<div class="login-page">
	  <div class="form">
	   <form class="login-form" method="POST" action="login2.php">
	    <input type="text" name="ID" placeholder="Identifiant"/>
	    <input type="password" name="MDP" id="MDP" placeholder="Mot de passe"/>
	    <input type="checkbox" onclick="myFunction()">Show Password 
	    <button type="submit" name="t" value='.$_POST["type"].'>Se connecter</button>
	  </form>
	</div>
</div>
</body>
	</html>';

	}
	else {
		header('Location: index.php');
	} 
}
?>

