<?php
require 'fonctions.php';
session_start();
  
  $session=connexionbd();
  if (!(isset($_SESSION['typeuser']) and isset($_SESSION["login"]) and $_SESSION['typeuser']=="etu" )) {
  header("location:index.php"); 
}
else {
?>


<!DOCTYPE HTML>

<html>
  <head>
    <title> Modifier un mot de passe</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
       <script type="text/javascript">
    function myFunction() {
  var x = document.getElementById("MDP");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
} </script>
  </head>


  <body>
        <h3><center>Modifier le mot de passe</center></h3>
        </br>
        </br>
    <div class="col-md-9">
      <div class="container">
        <div class="row">
          <fieldset style="width: 500px">
            <form method="POST" action='majmdpetu.php' onsubmit="if(!confirm('Confirmer la modification')){
          return false;}">
              <div >
                <div >

                <div >
                  <label>Entrez le nouveau mot de passe</label>
                  <input type="password" name="MDP" id="MDP" required="true">
                  <input type="checkbox" onclick="myFunction()" >Show 

                </div>
                <div class="col-md-12">
                  <input type="submit" value="Modifier le mot de passe" name="submit">
                </div>
              </div>
            </form>
          </fieldset>
        </div>
      </div>
    </div>
  </body>
</html>
<?php
}
?>