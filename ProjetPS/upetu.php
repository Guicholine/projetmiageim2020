<?php
require 'fonctions.php';
session_start();
  
  $session=connexionbd();
  if (!(isset($_SESSION['typeuser']) and isset($_SESSION["login"]) and $_SESSION['typeuser']=="admin" and $_SESSION['login']=="admin")) {
  header("location:index.php"); 
}
else {
?>


<!DOCTYPE HTML>

<html>
  <head>
    <title> Modifier un étudiant </title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
           <script type="text/javascript">
    function myFunction() {
  var x = document.getElementById("MDP");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
} </script>
  </head>


  <body>
        <h3><center>Modifier un étudiant</center></h3>
        </br>
        </br>
    <div class="col-md-9">
      <div class="container">
        <div class="row">
          <fieldset style="width: 500px">
            <form method="POST" action='majetu.php' onsubmit="if(!confirm('Confirmer la modification')){
          return false;}">
              <div class="row">
                <div class="col-md-12">
                  <label>Séléctionnez l'étudiant à modifier</label>

                  <select name='numEtu' id='section' onchange="var e=this.value;">
                    <?php
                      $res = mysqli_query($session,"SELECT * FROM etudiants");
                      while($row = mysqli_fetch_assoc($res)){
                        echo "<option value='".$row["numEtu"]."'>".$row["numEtu"]."</option>";
                      }
                    ?>
                  </select>
                </div>
                <div class="col-md-12">
                  <label>Entrez le nouveau mot de passe</label>
                  <input type="password" name="MDP" id="MDP" required="true">
      <input type="checkbox" onclick="myFunction()">Show
                                 <label>Séléctionnez le TP associé</label>
                  <select name='numTP' id='section'>
                    <?php
                      $res = mysqli_query($session,"SELECT * FROM groupestp gtp, groupestd gtd,sections s where s.numSec=gtd.numSec and gtp.numTD=gtd.numTD");
                      while($row = mysqli_fetch_assoc($res)){
                        echo "<option value='".$row["numTP"]."'>".$row["numSem"]."-".$row["nomSec"]."-".$row["nomTD"]."-".$row["nomTP"]."</option>";
                      }
                    ?>
                  </select>
                </div>
                <div class="col-md-12">
                  <input type="submit" value="Modifier l'étudiant" name="submit">
                </div>
              </div>
            </form>
          </fieldset>
        </div>
      </div>
    </div>
  </body>
</html>
<?php
}
?>