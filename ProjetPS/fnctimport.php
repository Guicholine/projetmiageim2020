<?php
	include 'vendor/autoload.php';
	require 'fonctions.php';
	session_start();
	$session=connexionbd();

	if (isset($_FILES["import_excel"])){
		if($_FILES["import_excel"]["name"] != ''){
			$allowed_extension = array('xls', 'csv', 'xlsx');
			$file_array = explode(".", $_FILES["import_excel"]["name"]);
			$file_extension = end($file_array);
			if(in_array($file_extension, $allowed_extension)){
				$file_name = time() . '.' . $file_extension;
				move_uploaded_file($_FILES['import_excel']['tmp_name'], $file_name);
				$file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file_name);
				$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($file_type);
				$spreadsheet = $reader->load($file_name);
				unlink($file_name);
				$nbFeuilles= $spreadsheet->getSheetCount();
				for ($i=1; $i < $nbFeuilles ; $i++) { 
					$data = $spreadsheet->getSheet($i)->rangeToArray('A4:C30');
					$tst=substr($spreadsheet->getSheet($i)->getCell('A2'),13);
					$sem=substr($tst,0,1);
					$sec=substr($tst,3,1);
					$td=substr($tst,4,1);
					$tp=substr($tst,5,1);
					//echo $sem."-".$sec."-".$td."-".$tp."</br>";

					$query = 'SELECT * from groupestp gtp, groupestd gtd, sections s , semestres sem where gtp.numTD=gtd.numTD and gtd.numSec=s.numSec and s.numSem=sem.numSem and s.nomSec="'.$sec.'" and sem.numSem="'.$sem.'" and gtd.nomTD='.$td.' and gtp.nomTP='.$tp.''; //and s.numSem='.$sem.'
		    		$result = mysqli_query ($session, $query); 
		     		while ($ligne=mysqli_fetch_array ($result)) {
						$ntp= $ligne['numTP'];
					}
					foreach($data as $row){
						$insert_data = array(
							'num'	=>	$row[0],
							'nom'	=>	$row[1],
							'prenom' =>	$row[2],
						);
						if($insert_data["prenom"]<>""){
							$mdphash=substr($insert_data["prenom"],0,1).substr($insert_data["nom"],0,1).$insert_data["num"];
							$mdp=password_hash($mdphash,PASSWORD_DEFAULT);
							$stmt = mysqli_prepare($session, 'INSERT INTO etudiants(numEtu, nomEtu, prenomEtu,mdp,numTP) VALUES(?,?,?,?,?)');
			    			mysqli_stmt_bind_param($stmt,"isssi",$insert_data["num"],$insert_data["nom"],$insert_data["prenom"],$mdp,$ntp);
			    			mysqli_stmt_execute($stmt);
						}
					}
					$message = '<div class="alert alert-success">Etudiants importés avec succés</div>';
				}
			}
			else{
				$message = '<div class="alert alert-danger">Veuillez choisir un fichier .xls ou .xlsx </div>';
			}
		}
		else{
			$message = '<div class="alert alert-danger">Veuillez selectionner un fichier</div>';
		}
		echo $message;
	}
	else{
  		header("location:index.php"); 
	}
?>