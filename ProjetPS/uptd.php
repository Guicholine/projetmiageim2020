<?php
require 'fonctions.php';
session_start();
  
  $session=connexionbd();
  if (!(isset($_SESSION['typeuser']) and isset($_SESSION["login"]) and $_SESSION['typeuser']=="admin" and $_SESSION['login']=="admin")) {
  header("location:index.php"); 
}
else {
?>


<!DOCTYPE HTML>

<html>
  <head>
    <title> Modifier un TD </title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
  </head>


  <body>
        <h3><center>Modifier un TD</center></h3>
        </br>
        </br>
    <div class="col-md-9">
      <div class="container">
        <div class="row">
          <fieldset style="width: 500px">
            <form method="POST" action='majtd.php' onsubmit="if(!confirm('Confirmer la modification')){
          return false;}">
              <div class="row">
                <div class="col-md-12">
                  <label>Séléctionnez le TD à modifier</label>
                  <select name='numTD' id='section'>
                    <?php
                      $res = mysqli_query($session,"SELECT * FROM groupestd gtd,sections s where s.numSec=gtd.numSec");
      while($row = mysqli_fetch_assoc($res)){
        echo "<option value='".$row["numTD"]."'>".$row["numSem"]."-".$row["nomSec"]."-".$row["nomTD"]."</option>";
      }
                    ?>
                  </select>
                  <label>Séléctionnez la nouvelle section associée</label>
                  <select name='numSec' id='section'>
                    <?php
                      $res = mysqli_query($session,"SELECT * FROM sections");
                      while($row = mysqli_fetch_assoc($res)){
                        echo "<option value='".$row["numSec"]."'>".$row["numSem"]."-".$row["nomSec"]."</option>";
                      }
                    ?>
                  </select>
                </div>
                <div class="col-md-12">
                  <label>Entrez le nouveau nom de TD</label>
                  <input type="text" name="newnomtd" required="true">
                </div>
                <div class="col-md-12">
                  <input type="submit" value="Modifier le TD" name="submit">
                </div>
              </div>
            </form>
          </fieldset>
        </div>
      </div>
    </div>
  </body>
</html>
<?php
}
?>