<?php
require 'fonctions.php';
session_start();
  
  $session=connexionbd();
  if (!(isset($_SESSION['typeuser']) and isset($_SESSION["login"]) and $_SESSION['typeuser']=="admin" and $_SESSION['login']=="admin")) {
  header("location:index.php"); 
}
else {
?>


<!DOCTYPE HTML>

<html>
  <head>
    <title> Créer un nouveau TD </title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
  </head>


  <body>
        <h3><center>Ajouter un nouveau groupe de TD</center></h3>
        </br>
        </br>
    <div class="col-md-9">
      <div class="container">
        <div class="row">
          <fieldset style="width: 500px">
            <form method="POST" action='addTD.php' onsubmit="if(!confirm('Confirmez-vous cette action ?')){
          return false;}">
              <label for="nomTD"> Veuillez donner un nom au groupe de TD : </label>
              <input type="text" name ="NomTD" required="true">
              <div class="row">
                <div class="col-md-12">
                  <label>Séléctionnez la section correspondante</label>
                  <select name='numSec' id='section'>
                    <?php
                      $res = mysqli_query($session,"SELECT * FROM sections");
                      while($row = mysqli_fetch_assoc($res)){
                        echo "<option value='".$row["numSec"]."'>".$row["numSem"]."-".$row["nomSec"]."</option>";
                      }
                    ?>
                  </select>
                </div>
                <div class="col-md-12">
                  <input type="submit" value="Créer le nouveau TD" name="submit">
                </div>
              </div>
            </form>
          </fieldset>
        </div>
      </div>
    </div>
  </body>
</html>
<?php
}
?>