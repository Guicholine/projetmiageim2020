<?php
require 'fonctions.php';
session_start();
$session=connexionbd();
$max=quelmaxdecol($session);
if(isset($_POST["back"])){
  if ($_POST["back"]==1) {
    $ne=maxexp($session);
    deleteexperience($session,$ne);  
  }
  elseif ($_POST["back"]==2) {
    $ne=maxexp($session);
    deleteexperience($session,$ne); 
    $ne=maxexp($session);
    deleteexperience($session,$ne); 
  }
}
  if (!(isset($_SESSION['typeuser']) and isset($_SESSION["login"]) and $_SESSION['typeuser']!="etu")) {
  header("location:index.php");
}
else {
?>
<html>
  <head>
    <script type='text/javascript'>
      function addTextBoxField(a){
            var input = document.createElement('input');
            input.type = "text";
            input.name = a+"[]";
            input.size = "30";
            input.placeholder="Libellé de la "+a;
            input.required="true";
            var container = document.getElementById(a);
            container.appendChild(input);
      }


    </script>
    <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="css/form">
  </head>

  <body>
    <div class="container">
      <form method="POST" action="tst2.php">
        <div>
          <label>Titre de l'experience </label>
          <input type='Text' id='nomEx' name='nomEx' required="true">
        </div>
        <div>
          <label>Résumé de l'experience </label>
          <input type='Text' id='resum' name='resum'  placeholder="Facultatif..." value="">
        </div>
          <div >
                  <label>Séléctionnez le semestre correspondant</label>
                  <select name='numSem' id='semestre'>
                    <?php
                      $res = mysqli_query($session,"SELECT * FROM semestres");
                      while($row = mysqli_fetch_assoc($res)){
                        echo "<option value='".$row["numSem"]."'>".$row["numSem"]."</option>";
                      }
                    ?>
                  </select>
                </div>
        <div>
          <label>Date de fin de l'experience</label>
          <input type='date' id='datefin' name='datefin' required="true">
        </div>

        <div id="colonne">
          <?php
          if(isset($_POST["experience"])){
            $colmere=recupcolonnesmere($session,$_POST["experience"]);
            while ($ligne=mysqli_fetch_array ($colmere)) {
              $a="'".$ligne['libelle']."'";
              echo "<input type='text' name='colonne[]' size ='30' value=".$a." required ='true'>";
            }
          }
          ?>
        </div>
          <input type="button" value="Ajouter colonne" onclick="addTextBoxField('colonne');">
        <div id="sous-colonne">
          <?php
          if(isset($_POST["experience"])){
            $colfille=recupcolonnesfilles($session,$_POST["experience"]);
            while ($ligne=mysqli_fetch_array ($colfille)) {
              $a="'".$ligne['libelle']."'";


              echo "<input type='text' name='sous-colonne[]' size ='30' value=".$a." required ='true'>";
            }
          }
          ?>
        </div>
          <input type="button" value="Ajouter sous-colonne" onclick="addTextBoxField('sous-colonne');">
        <div>
          <input type="submit" value="Créer la nouvelle experience">
        </div>
      </form>
    </div>
    <script type='text/javascript'>
      document.write(a[0]);
    </script>
  </body>
</html>
<?php
}
?>

