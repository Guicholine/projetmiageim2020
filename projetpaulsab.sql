-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 08 juil. 2020 à 17:23
-- Version du serveur :  5.7.23
-- Version de PHP : 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `projetpaulsab`
--
CREATE DATABASE IF NOT EXISTS `projetpaulsab` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `projetpaulsab`;

-- --------------------------------------------------------

--
-- Structure de la table `colonnes`
--

DROP TABLE IF EXISTS `colonnes`;
CREATE TABLE IF NOT EXISTS `colonnes` (
  `numCol` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(200) NOT NULL,
  `typeDonnees` tinyint(1) NOT NULL,
  `numExp` int(11) NOT NULL,
  `numColMere` int(11) DEFAULT NULL,
  PRIMARY KEY (`numCol`),
  KEY `fknumcolmere` (`numColMere`),
  KEY `fknumexpcol` (`numExp`)
) ENGINE=InnoDB AUTO_INCREMENT=978 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `colonnes`
--

INSERT INTO `colonnes` (`numCol`, `libelle`, `typeDonnees`, `numExp`, `numColMere`) VALUES
(1, 'Vessie dans le Ringer', 0, 1, NULL),
(2, 'Vessie dans le Ringer+extraits hypophysaires', 0, 1, NULL),
(3, 'Vessie dans le Ringer', 0, 1, NULL),
(4, 't0', 0, 1, 1),
(5, 't10', 0, 1, 1),
(22, 't20', 0, 1, 1),
(23, 't30', 0, 1, 1),
(24, 't40', 0, 1, 2),
(25, 't50', 0, 1, 2),
(26, 't60', 0, 1, 2),
(27, 't70', 0, 1, 3),
(28, 't80', 0, 1, 3),
(29, 't90', 0, 1, 3),
(30, 'Rats nourris', 1, 5, NULL),
(31, 'GLC Libre', 1, 5, 30),
(32, 'GLC Total', 0, 5, 30),
(33, 'GLC Glycogène', 0, 5, 30),
(34, 'Rat à jeun', 0, 5, NULL),
(35, 'GLC Libre', 1, 5, 34),
(36, 'GLC Total', 0, 5, 34),
(37, 'GLC Glycogène', 0, 5, 34);

-- --------------------------------------------------------

--
-- Structure de la table `enseignants`
--

DROP TABLE IF EXISTS `enseignants`;
CREATE TABLE IF NOT EXISTS `enseignants` (
  `numEns` int(11) NOT NULL,
  `nomEns` varchar(30) NOT NULL,
  `prenomEns` varchar(30) NOT NULL,
  `mdp` varchar(150) NOT NULL,
  PRIMARY KEY (`numEns`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `enseignants`
--

INSERT INTO `enseignants` (`numEns`, `nomEns`, `prenomEns`, `mdp`) VALUES
(123456, 'LORSIGNOL', 'ANNE', '$2y$10$qUmzSIrWWt7tBWgBlmxBl.vZ.JjEnxiXheThlZUYtOpgOmpkqx7Je');

-- --------------------------------------------------------

--
-- Structure de la table `etudiants`
--

DROP TABLE IF EXISTS `etudiants`;
CREATE TABLE IF NOT EXISTS `etudiants` (
  `numEtu` int(11) NOT NULL,
  `nomEtu` varchar(25) NOT NULL,
  `prenomEtu` varchar(25) NOT NULL,
  `mdp` varchar(200) NOT NULL,
  `numTP` int(11) NOT NULL,
  PRIMARY KEY (`numEtu`),
  KEY `fknumtp` (`numTP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `etudiants`
--

INSERT INTO `etudiants` (`numEtu`, `nomEtu`, `prenomEtu`, `mdp`, `numTP`) VALUES
(20000000, 'test', 'test', '$2y$10$iAop1Rhk.tSicHpJyhf6UOrm9RZ8DzTs8ssCain.OaYUweWeetoFi', 8),
(21403709, 'AMRANI', 'IMENE', '$2y$10$i4qriub2LauNunhfRb8Xy.uD4tHV8ierXOKN3NqYsaBEbRmaIkXcG', 11),
(21602030, 'HARROU', 'MILANA', '$2y$10$4WBg3UHd9/cHIWSQCR5BeubMqXcRSqBJ0fUmL3pKPo3xEYp9Lb34C', 9),
(21611278, 'AMODE', 'FAIZA', '$2y$10$/5OL3ubt/AV5hFqdjtVldeNtEBvSyNJEpxaYu2r2c7n0Kf8OTROf6', 15),
(21700245, 'BENNACAR', 'ZAKI', '$2y$10$hVLMKqLSpxOexLI02ath1.9xIn3q79H171uzvNFqRSeYOVhrmZYGW', 9),
(21701356, 'GONZALEZ', 'MARGAUX', '$2y$10$5arCI2jJt9IGxIYH2kF9BupA20S6hH6UgFIUfzrA7iaSzdqueQk6C', 14),
(21701642, 'BENYAHIA', 'RAYHENE', '$2y$10$WtygX5oFBEbJEb6PxauAwOfevMohUakFJq2ormwZv916hdVn4vZzW', 11),
(21702006, 'ROCHE', 'AXEL', '$2y$10$c7AcC.Zhl9PirzM8kQ4cb./uycUoCrQzObVI7tQJy5jQ5cnsnwUui', 14),
(21702106, 'TROALEN', 'PAUL', '$2y$10$sM/zhQ3jTSf3jhSNWqF9luC.GsKmGyDuvL88JwYTaaEFrPPjLsCBa', 11),
(21702542, 'BONADEI', 'OCEANE', '$2y$10$riBHNTwnvjRRv5nb1T/up.MiwJoAIwuBk1hDKs/wpRSDURMaqHv/m', 8),
(21703464, 'CHAIK', 'AHMED', '$2y$10$zEa0c1ieNILjUF.IMSlt.OD.W.b5887/iYDlxzUzRGINcd/k6.isS', 13),
(21703794, 'NEBOT', 'LEANA', '$2y$10$iZTOHSTTUFdT2QdHeyL35u6uGSdutvVmvJQgzijdJZdjtRFvwxzZ6', 9),
(21704177, '', '', '$2y$10$HxdvOLLmoa2wNciz1hWqqOjN71PwGlE1XXfTIPqWlzOK62FQDq.qC', 1),
(21704499, 'LEON', 'PAULINE', '$2y$10$sjf.NCQazQsFxcu9aWGEuuoWOv5L.e7EDdXjFitLe8SbWrEtOMi8.', 16),
(21704626, 'HAZIZA', 'KAREN', '$2y$10$.IZyXw.EcwLf88SwGSaTl.aAkFKJ1/q16khlJrwmxofEcfY5VxhO6', 17),
(21705588, 'LEGOIS', 'MAELIS', '$2y$10$WkvAuGuwxikC7SZFUQvLZO5zUCXfSvezRY7uPYC23qRYSE11RaBo.', 17),
(21705607, 'LEVEZ', 'GAUTHIER', '$2y$10$sxXADj8kTAIq6R54lI.MSOlpwbxk4ssw3KbprijojlhFmLMzDa0zu', 15),
(21707008, '', '', '$2y$10$Tcidc7NUp.QB2ETWZ8cXS.EH3v7WMdl6K28pXILlg0UMnEwwpwbeO', 2),
(21800368, '', '', '$2y$10$r/7idnDu6udnmXEbl/A5xuvgxt9wS8wWpKUk4wB6Am6R2N1krQV86', 3),
(21800567, 'ELALAOUI', 'ADAM', '$2y$10$3dK8W.jf8ntsJ0PVbqPJxOTftW4T9E3B7XRgc1XSN.VugBr2mCpXa', 12),
(21800938, 'GRELLIER', 'JESSICA', '$2y$10$xoNRxR6puW.pH3JNacv1jOMzvbgEMj0uIksr7GSWkWYMTwQlugsei', 11),
(21801044, 'COMBA', 'LEA', '$2y$10$KghDiYVopp3WjYeZJ9lDm.m0NiHW2Yzh5jHwWFm43vXONzuNAotyy', 11),
(21801567, 'CHAUMARD', 'SOLENE', '$2y$10$Oy/MFMPwTYr379MPHq0RIuy3f2LxDirsQgMUPKi.KqtSw/xnO5i16', 14),
(21801742, 'MAURICE', 'MARGAUX', '$2y$10$hNsDHItPcjqrr6Rg0fSxreStGoK3IbZZa3/Emi3ELyLybtvSTKnfy', 14),
(21802125, 'CAVALIER', 'MARVIN', '$2y$10$kzuszcE0csYkubi476sTTONoOfkSv0438nB4SpL5qKr7Ww1MJnZJq', 9),
(21802257, 'ESTEBAN', 'LUCIE', '$2y$10$S6WHmJ3iq0m00fXjOwfSFOdP51Q8N/h3hN1/Rp3LBHwz3rrCI7pAm', 8),
(21803337, 'RAONIMANANA RASOLDIER', 'SOAFARA', '$2y$10$1.pH/q2leKKPbfW.6ItzQ.DJgAwt95B3vQ1lUWkfS6NW.jrPMtv5e', 12),
(21803476, 'DARTADJERI', 'HADJER', '$2y$10$WIOHWiNJPeSXXaNL3WuF1enn1akCkTJFLNfkfzM18lTTLjk23PBAu', 11),
(21803518, 'KHADRI', 'LILIA', '$2y$10$0b9iKYl3.hrZPKyxadBPBOfriD5ANtrlLGb/Q7rCNm/YXL1Stdwfe', 10),
(21803522, 'LOPEZ', 'LENA', '$2y$10$nWVmqXB1iTJVDwmTkCNW4.hX6F0J5pXA8kwfUTjCzGZ0xey50wTQm', 8),
(21804218, 'DOS SANTOS', 'LUCIE', '$2y$10$f0GybVL9.11yqE15r2GbVeIlVk0.eap1BCM7Cd4ybx3ywcWmg/js.', 9),
(21804562, 'CHAPELLE', 'JULIEN', '$2y$10$M3KnOtvIS0QBci1/lYAXy.klVNemYl5CvyD17ac/k0wgku7EQfXp.', 15),
(21805007, 'BOUTIQUE', 'GABIN', '$2y$10$YhXppKd3o3ECJrAcDigrp..kEUVi5vLmguWk8pLe/YMSfeF.Na8Zu', 10),
(21805653, 'EDJEKOUANE', 'FLORIAN', '$2y$10$fsZTyriZvBPCybdgadjQk.70Y2jHVoOAaaZ2ewJD2quN3wrNI5Wlm', 9),
(21805700, 'LAUWERIERE', 'ELODIE', '$2y$10$VSFDWpR9MGbVjBB5khBoquAPP/ecwFNxxY3DN8K4VfrvhTkkkqmGy', 9),
(21805839, 'CHERUBIN-BAMBY', 'MILENA', '$2y$10$AmbE/0xkXmB8uEe23TdE7OkCb1UtqNYiLp.M/V8EvuW7GfI2Pd4i2', 16),
(21805935, 'BONIFASSY', 'MARGAUX', '$2y$10$4qd8F.7dx.EL85Om6fs8huRaKVd8rzOWTx0eETPmQAw63lFZp7.zG', 8),
(21806442, 'BOUTAIBI', 'BRAHIM', '$2y$10$7SBOFgTE1wec56yR5umpw.6A1PvDe/A5T1pqcrTc4r9re8D5WVcVi', 15),
(21807100, 'VALEAMA--VITRY', 'SAMANTHA', '$2y$10$ZqYq1ax0agrXz2pEmkkTsOzKgrx8K9mjNk39p/squjopHgKHS8/oe', 10),
(21807967, 'SADFI', 'SAIMA', '$2y$10$b.BksmwcaVKJwpc26dLnXOlQGqZME/qbYU326/JxRjLZOT5qkrYFC', 15),
(21807998, 'DELINDE', 'FIONA', '$2y$10$vAVv9Kr/NaY6yivz4vJC7eSb8Guo3ekLp9cLRvZJAwinkv2aNtzQG', 16),
(21808663, 'BARRES', 'ELSA', '$2y$10$.fBCOiUgwh3JBpzSGK.jEurQj6UqTPlUSh8uFfKIUFofeb4btck8.', 11),
(21808979, 'ABERRANE', 'LEANA', '$2y$10$sQwJalMfZhgX/vdgxAPZhePCiPrG9SEWGjPJnEndmFHDkjUcCANLq', 12),
(21809041, 'RODRIGUES', 'JOHANNA', '$2y$10$BgJEB2zljBJ9CoUqd7lNpOmZ6f5b3vfKLNIEoydzupuB2G8qPPcRu', 10),
(21809105, 'AIT M\'BARK', 'SAMSHA', '$2y$10$axfly2SyyYQyGHba18FurOa7OWnAWo92b5AJMgPEAp8yrmvhwID9W', 8),
(21809208, 'BARHOUMI', 'IKRAM', '$2y$10$J3x2Fe4MXeEf.eG9ii8Jju5rTDVSjhbGUEoEKX0.QvNM6E2AMNypC', 11),
(21809450, 'CHMIMO', 'SOPHIA', '$2y$10$qy5B7/JtoH5eioY.ZnzGfe2Ib3TAdMrKtivUbbPLsW3varnHET.3m', 8),
(21809943, 'RATSIMBA', 'DIANE', '$2y$10$/BpJpdSyxolaiS4iziCKseNsQg.puPbPt.wX2F/B5d537Tz7DVzFu', 12),
(21809957, 'JEHLE', 'MARIE', '$2y$10$SeQOprm/V4ihXPNTrq4Wx.shGgyQJL2j.y/YTsn/j3/LcvetRv7zi', 14),
(21810103, 'BOERSCH', 'CLARA', '$2y$10$8o0buK6Vn./WGT9MRnwKduB0mxA9GBRSIOn.1SvGkFtTnBACOpxgy', 10),
(21810506, 'LE QUENTREC', 'JENNYFER', '$2y$10$C/UiaqlXg/E4U5odBT9i4uhGbVt2SrZinVuKz5JPGTsZkFKahShIG', 14),
(21811745, 'TECHER', 'MARGAUX', '$2y$10$6LDbV8MIjG9L8DWtJMxnC./.zAZDpooUKi5HmrkCc2Gz00FCBHF.y', 10),
(21812804, 'BEMI NKOSI', 'DELICE', '$2y$10$mQ2lTZcPYUW2qY8TEUiT3Ot10Bge/5Ddoxfq/DQEdcZHFAgVYPtqC', 9),
(21813587, '', '', '$2y$10$Yk/BYOd9wpY0IlhvdPo.1.bUUnVSXu.XRu5emin3zN9aQEJ5xNCoe', 1),
(21900086, 'LATTES', 'CHLOE', '$2y$10$OxDuhzBP92JhMoCIq39DrOlhH32TE7bWChjgIbILago8vP94YQ82S', 9),
(21900115, 'DJOUHRI', 'AMEL', '$2y$10$sBzJn1biItEDSPWfZ6Ncle3tI3lmLdizNe9mM97RNM6IyxjS/pGYa', 11),
(21900147, 'GONZALEZ', 'NATHAN', '$2y$10$rILegNK3NUy4dSvVG9FwVerbwsdm.yH56TXhprzcwsy7ugiaEI99O', 14),
(21900267, 'RUIZ', 'MELODY', '$2y$10$rDq2WAy84swVZKZ83q7S5unJDlrQL.W6hOUtHlvHnobDXhzqA9jP6', 15),
(21900299, 'GUIMBAUD', 'LOIC', '$2y$10$VDXz79QFhfgVGVKna8THHe5dWUZY/sTqSAigqXJFqcTLHKvf4pCTi', 10),
(21901061, 'REIX-VIGUIER', 'LOU-ANN', '$2y$10$Cj9bKGdsBMYlgafWDqDOs.lGvArP0IdkuK2uIuAEQ4rOCTepO8MSW', 17),
(21901243, 'COSTES', 'ANTONIN', '$2y$10$od./PIpcORHG5ZAtALfuXeP7ICWdoVtFBRZGA2SQzhpRFue.eM70q', 8),
(21901346, 'GARREAU', 'ANAIS', '$2y$10$5bs5Rgi3IeM3ucdYV8wQje5877wsj63Ak0mj5rBmLdu7.heJt/xeG', 16),
(21901450, 'ARCAS', 'ALEXIS', '$2y$10$mpSzO6Wrscx.bBqN7pYNrOCgx33w2rMxPtG9yM1FlDz81hCqUTS/a', 17),
(21901658, 'TALIPOVA', 'RENATA', '$2y$10$v3HE6w797CwAUoMkcQ1Cb.vG4Ux13.hTvL/N6p77r9Uly4Oey//jC', 16),
(21901664, 'TESSAROTTO', 'CHARLOTTE', '$2y$10$5bQPTUvil6d/r88URgkdl.EWl.yhgmH7dTwzprAQ7MPDq16fyjbE2', 17),
(21901744, 'DELAHAYE', 'OLIVIA', '$2y$10$GL2ssHDQUH0GsObhIeoDUeYoQ3G/hQtwz4nE1ilvHeeZpugnHMdqW', 10),
(21901763, 'FAZZINO', 'LISE', '$2y$10$kyrfRwFsEn29JhugRurNGesRGXZL1VUWu0f/EAKt33slmC6Eu0Kea', 11),
(21901904, 'SASTRE', 'LEA', '$2y$10$rchMJZ9.kkjm7KpMkbyXB.iJZHYpRLye36uip3wu2Xnu.kWQnxD1u', 16),
(21901974, 'BOMBEN', 'OCEANE', '$2y$10$FxQ57oejP1Z0KI8zfrnsQe8Egoo1ZpFIVcrju/RDwFcYmYiYIOWi2', 12),
(21901995, 'EISELE', 'MAURANE', '$2y$10$QbpsReAW1DNBZeNaHillguAUcZr/TS3U7ou.u1pBg1zyOuUCQAO12', 9),
(21902018, 'PERRIN', 'LOLIE', '$2y$10$glo2T7UG5F/R0sUMBR/KY.73mybjsPBo6zNk63AwsLatzTprFbFsC', 17),
(21902145, 'MARTIN', 'ETIENNE', '$2y$10$UUMDxJ.A89IAcyAJoy8EJuZ/lTCMdEBG/rpn7Fvmfzawf2KktnwuW', 8),
(21902425, 'RAYMON', 'LOUIS', '$2y$10$3uYnwq1xpH4N9RFTI4gR6O.8.ih9Yyrv0bRCuOR.3B41hp2.MvDlm', 8),
(21902670, 'HUC', 'ROMANE', '$2y$10$A8QQY.L9SqxIBqlFDb3OVOoQns4lh4w711ir.0TLGDAmhcrElWeQy', 16),
(21902715, 'BOZZO', 'FAUSTINE', '$2y$10$esAC1GJkpHL.E0AIiRBpkuonobBfvBeoEMuZHy5NBbWoE2YxYUSD.', 14),
(21902927, 'HURPY', 'SARA', '$2y$10$Cxjne/CCv4BVIVX7sOvXNu0A.FZowmpmu4FPZhwNSySHWC4dZM.pq', 11),
(21902938, 'JALIL', 'NAJOUA', '$2y$10$Na3D721MfMrgUBpbvRvVJOG9jWKCx2iT4xzq.4YP3LggMqirGVEBm', 17),
(21903170, 'CARILLO', 'NATHAN', '$2y$10$3LJn3Bs8l7P9iEqTFt/pe.n6ufJx32HKaSZ1cu3YLh69Gy3Ls.BBm', 10),
(21903260, 'LEDAKI ENGONOPOULOU', 'ELEONORA', '$2y$10$HKZvw4RYwvIUCaQLY54EweoC7hB6hKztoj2wx9tXomd4sNp.9Nhzi', 10),
(21903488, 'GUEDJ', 'EMMA', '$2y$10$UV3oPjBUrhwSe9648NUKjOcX7XWWla0oyjngrTLqoWwdOQTpNl9w6', 17),
(21903549, 'ANTIGA', 'MANON', '$2y$10$/5B.hGJ09App0raMte4yyOSYvZZqpr2w4zNP.IpWMaLNS0tO6u.Xi', 11),
(21903622, 'MANDROU', 'CLARISSE', '$2y$10$RPaZlyCWqZ0BrLD2Pu3hmuuYHkPHdghe4HNzzmYP/4dbJwdFCZIfC', 12),
(21903685, 'RUFFATO', 'KENZO', '$2y$10$YNOSTi4lt/85vsM5NPimXO3YBpRM2JgnhPsuGPCoyDJmLVl5XFoJO', 15),
(21903725, 'RANCILLAC', 'CELIA', '$2y$10$HLQUqH/8rZ8c6YLO9PRqXe3MpISxoDxgvGyhacKoHI.o2BgFugyze', 15),
(21903796, 'VILAPLANA', 'LUCILLE', '$2y$10$RO4mPEqouqrpG.7ppAx2gOXk7lt9dyVmyZ1mON4hI9vAvJb3H0cHa', 16),
(21903929, 'RIVES-ARNAL', 'CAMILLE', '$2y$10$j4gF3bGRY2X5ibC0glaVHOUJ8v.ByDf7pHre8oSpBTGI5.zGTIEUm', 15),
(21903932, 'BAUBEL', 'ALICE', '$2y$10$dMlW3kv6RgmISwHsG1yZmeH8kZa74GX7ALH0q7nEm.oER4J1NxL2e', 12),
(21903940, 'CHEVRIER', 'FLAVIEN', '$2y$10$QseRYmbXX9onm6YKoRBswueEXC.59e1ir.ke7IceeuHQ5CuhvtZHW', 15),
(21904014, 'DELENCLOS', 'NOEMIE', '$2y$10$IGfTSiaQMP.2NFgnyjj4JetsBtt2PdURpFUGXzNqjqTGifO8jmwMW', 16),
(21904893, 'CASSARO', 'MELISSA', '$2y$10$jUj.bSaZioBjUlzJeoazQOyv/ZSFmlVuPboc2RpiZ8Lu1NbXjIVgC', 11),
(21904910, 'SIMONET', 'LUCA', '$2y$10$EHActSxNZDILRYTmVdT2iulamWT1Zcvt.E0v1gL6oErFjiBtfMTby', 15),
(21904944, 'CONDEZ', 'MARINE', '$2y$10$alefLKv/s2piOweF.inVw.n53wHU4VJ2Eme3LqDRuht1simjG7TgW', 12),
(21905035, 'FREHRING', 'LOU', '$2y$10$B/1E7DAXPipKc7WDPpw/6.B2dx5REI7bDyYKwk1WYz1r1BhHCQAzm', 9),
(21905095, 'POMIER', 'COLINE', '$2y$10$09loa1gEaJBkEgPlheuNROpX4GAA39BN9CqW3Pqxx9oepa/DOvGQu', 13),
(21905128, 'BOCHERENS', 'ALICE', '$2y$10$Lp3ZHaBxd5rgytMIMb.nyu7xiUnFzuWOQOpaIedZch5cTVTjba0Ri', 17),
(21905218, 'ANGLES', 'NATALIA', '$2y$10$eM0JDh0U/mpNP/tTBqxVQOh5D5ZUvkY1QLaZtn5USEL6EpsuEGVPm', 11),
(21905289, 'VALLEZ', 'REMI', '$2y$10$uhRQeEyKwftrcXaR5Fl.S.XsUP9SJSJ7FjA2XLlgGIjEDZGty4Czq', 16),
(21905401, 'AUTRET', 'AGATHE', '$2y$10$rQX6vG3Is0gPz6lsSdSnIObrRiwRIEJuLNVsL4bC98be9sN36q19q', 12),
(21905491, 'FUSEAU', 'MARGOT', '$2y$10$GEDaKQtZKiKmSQlAGEk9Nu5IuMnia7Q022LR0qHpQZbzNLK0TtgKC', 14),
(21905516, 'BENSAHA', 'AMEL', '$2y$10$qzJ447Cakn43kHR69j423e0oRAOS50CBbpiFmnMvjaLhLQQCKLjlK', 9),
(21905642, 'RECOULES CAPGRAS', 'LENA', '$2y$10$hccuycyS1UXLik17r6bKEOu/pHBwazUaWvdfqzRWSE4.rqwIEGHzi', 9),
(21905765, 'CASTELLO', 'CLARA', '$2y$10$TroD0MpwjlN/K5YnoNmvIukhWmpGl/dTcBytfJIeBCiyRVAYUuySy', 14),
(21906097, 'RODRIGUEZ', 'EMMA', '$2y$10$dNn0EdbrUmcTXDNZ6ClQde4FWtaKgYVF1G5Q7Z.zOiN6FaknkGAgS', 17),
(21906248, 'CAILLOL', 'ROMANE', '$2y$10$7.nqtA5TBrJKbR0v20pQROfvs9RXe37eRgrPN/wCgbxYAUQaFRWzS', 9),
(21906375, 'LOZADA DOMIC', 'ARIAN', '$2y$10$Qp8mbEvNSX55A8rwf8KSxOVa0SXlWeuOfPqJAyBVUWgH86hPPS7fe', 14),
(21906406, 'BOUDIER', 'CAROLANE', '$2y$10$nNLBUIWQvac7PM65HGLW/uYM4TGJAw9lkMdx2vmhoPcSbli84WOz2', 15),
(21906471, 'ROUX', 'ALICE', '$2y$10$u7MTlPq8idrWgnPnUXzaeednapk0ssGX/cTGI1I.jX185nv929vKm', 13),
(21906853, 'SACCAREAU', 'EMMA', '$2y$10$zeZyJtzpdm6SlsRaYMJ29Ojwcc4Cn/1V34WjL/9yv/GjOBS8ejBEe', 12),
(21906869, 'PONS', 'LUNA', '$2y$10$RZ8SNCqOiMfy4KfmeVs7bu2Mk5VIWDHSeZmTdlkdp7c9EBxDJ8NhK', 13),
(21907067, 'DANDINE', 'CHARLOTTE', '$2y$10$v97JkKEIOYAHU07xPLO65.aOCpRRu5XnWDjE/c.42dgiyMcipYSau', 13),
(21907195, 'ROUZE', 'ALEXIS', '$2y$10$ZACaskQaK2Si.sI/qV.VU.cviVGitL51Q/dV4EuDGtK5eriZwoE1K', 14),
(21907261, 'RINGUET', 'SYLVIO', '$2y$10$JMeqjjpdpIj6fDnbFCq0oOT5kfXsjRs3XQlpL43r36CEh20e9ozMO', 13),
(21907719, 'HIDALGO', 'LEA', '$2y$10$47cqIwMOK.6Ex5Ekb2/Qx.hCRE8Cqt5IX8ReQzYcALumMkdsObLr2', 17),
(21907892, 'AZAM', 'CAMILLE', '$2y$10$2ZxLufLM5YYrBQAi7G3WFe6pCfr.M733U6o1c38ozsPmmqF3fuWuu', 12),
(21907916, 'YVERGNIAUX', 'AOUREGAN', '$2y$10$aI3.jjoFLKndBT8KxWFWaudPPzsh8Qg4Yka4eDRRexsPLCsv9EYW6', 9),
(21908195, 'MAS', 'LEA', '$2y$10$/0tH98c9t0/ZA5SMKv2ClOk9u/zOin5kAGUNpvx/5fgNGt.8AYMCa', 12),
(21908371, 'NOGUES', 'TOM', '$2y$10$vEY6IAlXtZq3F4tcNnmPXukACdG8vwv8/D4C0Sdr4ME/PCzLqE6rS', 8),
(21908401, '', '', '$2y$10$RJPM.zSxfaM68aOEi9TDHOYXkDlWEJHqwAAI25m0xw.AWq77XB3ta', 7),
(21908407, '', '', '$2y$10$QDVXZRz2Z5nwY8THSHEBDevSdWsmfU6/OCqiCRTJxG1i3AA/YVU6q', 3),
(21908702, 'ANDRE', 'LUCIE', '$2y$10$EY9uVqDnXy7ZaLljaO2XtO23GR7gTZJiPwDq198acV/WMQm28Y2iO', 11),
(21908710, 'FEDRIGO', 'CINDY', '$2y$10$9c6QAGLZLhivKFQaDP.9NOs6d7llkOUmeRzRBcPuOMExA/aqTdNN.', 17),
(21908750, 'BOUDEVILLE', 'MATEO', '$2y$10$4R3F6czzhFaUUrIXY6XRZev4PQ.sfmv5sa3i/x9c8JFlQiv819dRi', 8),
(21908796, 'RIGOULAT', 'CELIA', '$2y$10$.fmglXrJj5RQdBFcQS4rCeLAZ8mNr1vhFzHFZhxED8Nrwt4AgM8Yi', 14),
(21908885, 'GINESTE', 'AGATHE', '$2y$10$NrE0JEzpDf5kZUq8w7CJxe7h9h/ZHxYRWyBOBiZFsYmfYnORnGvkO', 8),
(21908989, 'BERNIER', 'MATTHIEU', '$2y$10$jx/kQ8SryzBID.DEIKFojuumZv4dIXphWsSLvIsGjjnbBtqgxwK56', 13),
(21909108, 'HICKS', 'BENJAMIN', '$2y$10$/M5rPsVhOUXtsJbvZ5RBre2g.PKFfoAVqj6ugKhZO5gWXolPK63iO', 10),
(21909177, 'ARIES', 'LOUIS', '$2y$10$AfGnQk95FyPHwP3HV4w/4ehANllNmWaG4xED1bKttPjsGiFPZUFji', 17),
(21909290, 'CHASSAIGNE', 'EMMA', '$2y$10$mTEovE3yox/IBVJV.TuFFegiMTJYb8awGxsWLyGA35Ypz6P0NwAdG', 17),
(21909296, 'BOISIER', 'AMANDA', '$2y$10$SqMLnqUYggDdMcmm70Bx.ez/G8ebf.KaqR/YlJZArG3i8wNRQUTF.', 10),
(21909308, 'LANGLAIS', 'ANTONIN', '$2y$10$yPbH5qYI5v7WwTTKHpbNdedKO8UiUFyKO.U5GGg3jW2.pNkFHR7SO', 15),
(21909347, 'BELIN', 'EVAN', '$2y$10$S1llzhULenGTanKcFlHTjudstJlcIgHZyvVuHxm2p3tXOAFsQgzm2', 10),
(21909372, 'PICARIELLO', 'ANNA', '$2y$10$ZZv5ov95cozKSNI9.w0mhO/uJbZlkVX494LZ1n3iapKW2PKQmoFGK', 17),
(21909570, 'FLORES AGUILAR', 'THELMA NICOLE', '$2y$10$UsBFj9cmhqcacJnjEw6EK.94t1n4fB/SECEdZ3NgIEkWEAmJbkCuO', 8),
(21909627, 'DARNAUD', 'FAUSTINE', '$2y$10$v6f6tt/roDWS9SS5Aq2Kpe1SIoXyexPOGg9Cy4owt5kxtT3BLcHVq', 17),
(21909690, 'AUDEMAR', 'ELODIE', '$2y$10$RIMgx9xtHHYxuVSLV0PNM.Z.i9jOrKNQVGWIex7yeFLOxVy.q9AUW', 17),
(21909695, 'AMRI', 'AMEN', '$2y$10$JM5edT7zvUpVP5uwiUmit.nmr5DlnwRCUKy.oekTJpcXgQ2U6j.aG', 11),
(21909712, 'ALARIO', 'OCEANE', '$2y$10$ACo5BSmgJ5OwUoxzeXm6E.1VkzmE1iP7XnQvrvw3gUenmuoUVMyI.', 11),
(21910251, 'GAUSSERAN', 'CAMILLE', '$2y$10$7AGCc4Eyci0rrKLMbhIiQuArXTe4KuLLuxCa7uP4Kr6MSV/jqqQFW', 14),
(21910270, 'GUERIN', 'NOEMI', '$2y$10$YzDt7.ty.xJaSFfxeycOROSjWAtSXTbbzncFFF0rxJ7gFUYF2D1S2', 16),
(21910276, 'PETIT BIDOYET', 'SHAD', '$2y$10$bpeHO.IiRZijKO1qDTyQmeQhGdT.VILAdrMAG3tvv2zFnXbHE.ive', 15),
(21910310, 'BENETTI', 'THOMAS', '$2y$10$vj1issyo2RQzuSamW0asruhAdVRNNlI/2SdNtex7P2a3zWmPHhW..', 11),
(21910471, 'FOUILLADE', 'ROMAIN', '$2y$10$FsPvGaAelf4kSutMXM7f/O4Zmo1wbdksLj.kfZ4kOthu3ip307p6W', 15),
(21910507, 'RESPAUD', 'VALENTINE', '$2y$10$TfQriPcBgJN.uZ.ZaYPE6uMbNrWkX2wIOsNceMD3YtBPmfvhRVQAy', 15),
(21910814, 'HOARAU', 'MATTEO', '$2y$10$cdmorox.yspD6anEfvuUXes1LX7ROj8kfI21SrSwlNXgEnIU/j2SG', 10),
(21910885, 'AZEMA', 'MATTEO', '$2y$10$TzEISTsRBUuxSrPVmKQ2HuNvvmvK6VLvZReZ6/jxdRmXHA5ojG7Oe', 12),
(21910961, 'BOUBEKEUR', 'OUALID', '$2y$10$U2IA3XbFA025TRiXySliheCEmdXwMuQ2gb296sVV7Ak7vMySj1yXm', 10),
(21911191, 'CONDE', 'ANNA', '$2y$10$VRyzL7hftT8Fo42z.0mPa.oBPaNE1YdfEEWuy96ATH8W2B/UQ1dny', 12),
(21911192, 'BLONDEL', 'ILONA', '$2y$10$CbpR7NYZDnp9jI83G4yWS.809OWnRkbxaiXKuJgPGEntbihJyK67W', 16),
(21911242, 'COQUELIN', 'CAROLINE', '$2y$10$4yd14.J23QmMb0HyktHrxuKlxc4zRpJb7Q8XeM5.KWK1ce5d1vUBy', 10),
(21911247, 'MELANE', 'SOLEYNE', '$2y$10$.aEZXj0feAI74qqX4GV1f.clsemMUy/gGmTTw9ERva9QTkihRuGRK', 9),
(21911271, 'NAVARRO', 'MAEVA', '$2y$10$VqriT81UNdGvkSW5XXmk/.4NRPXWMejoHWpVBQtztuRLBEZN/WfDW', 8),
(21911294, 'BOUNAB', 'NAWEL', '$2y$10$E4iT84TRApeNjxdDhjFf3eVFZP3a1yWUD8ZVE9fHCGhK5k.dSQ4VK', 16),
(21911394, 'FERNANDEZ', 'LAURA', '$2y$10$auKhZZ.BkK9wQw3G4TV7hOdNL3Ajh.Ei142/H8/OcVQ9734LKriVG', 12),
(21911482, 'SANNEH', 'HAWA', '$2y$10$Crl7xL1cF846x55iBKr9tOQiHORNVoEA1D2bE9KRwZAgXckPCRCkS', 14),
(21911503, 'BRETON', 'ABIGAIL', '$2y$10$wGlp3V5bICcnTtfXLRIWjeDMAA5grDPb7Ns1xuVxyT207/6L86sG6', 13),
(21911515, 'CHALAOUX', 'JULIETTE', '$2y$10$6djJNOhIXN13fwGYCkgvgOEBgiepkmXcPyHZARKOoy6Vdls5u4bV2', 13),
(21911613, 'MONTIES', 'JULIEN', '$2y$10$rpb79zGbpeTuz6D3Unq7b.Ox6mwLlWeAZBuSNtThRD.5INlze4.Yq', 13),
(21911732, 'ROY', 'LOU', '$2y$10$RAVnQn/r1LR3B6kk01XdouA.wsCGQR2G/iusaW5lOVnqAXkmqCeWm', 12),
(21912064, 'EL MOSTADI', 'LAILA', '$2y$10$6ZQC6TQijXtU3EG3Nzwr3ej5qA/R97sqkgGCOpzVpZjKTJhdTCG06', 12),
(21912145, 'FEHRENBACH', 'ARTHUR', '$2y$10$Kk3q96Juk1D00kDq8tLqSe7B9oIbmYtRjBhDRW.VxDMiIeaBigExu', 13),
(21912258, 'LEPASTEUR', 'SONYA', '$2y$10$YOHJgCif.YaUf.T3RWV.jOtrYV9fDmEHtNFsVRydw2NeXvu8z4V7.', 16),
(21912278, 'MARIN', 'JULIE', '$2y$10$nVKX/dzZp5P7fHlHp.c7SODGQ.6hRbmdQXYYmBUMrP6c9uZc7e60u', 13),
(21912305, 'CECILLON', 'AURORE', '$2y$10$Mx.3.QufD34cbF4wJYlA/.Lb7LWU4U5MCkrbuvstWlgKrclGGELM6', 15),
(21912452, 'HASSANI', 'NISSA', '$2y$10$2ihuT/RdPz1Bpea/4JllPOStmUmUDT.n2w0Tnu.om5hNdN48o6LzK', 13),
(21912873, 'CASELLES', 'PABLO', '$2y$10$68pw9Z8BMlKnCWGkBAuCOejuTZun4LH85Mi9KkM75qv/.oi8ALTxa', 13),
(21913471, 'CHERON', 'COLINE', '$2y$10$Z.huO7hoz0sToksQm3kv0.IpTs9XUD34jSievGoRNGTz5aIxVQYsC', 13),
(21915349, 'BELLON', 'HELOISE', '$2y$10$J3cld.gf2NrEC4/9huBqO.ScEXsGhA2pAnwmL6BZerMVhHtYJvEj2', 13),
(21915479, 'BERNADETS', 'CELIA', '$2y$10$cQUrszOV1g5U5TrCFx4Sk.NEltSoPnG9eMXkRUYYaiB4Uj5JUH/3O', 14),
(21915907, 'BOUSSEL', 'MILENA', '$2y$10$Cce9.ldpH80gsfl7AB0d3uj49k0nEYTqJhlgeuXOodwXlCwXS06O2', 14),
(21916570, 'BEZARD', 'DEBORAH', '$2y$10$yIiHR6vPcbpR1I6ZYGHPwufUAOYwb892.7jtDh2U8twSOtYtNeokS', 8),
(21917714, 'RUBI', 'ALYSSA', '$2y$10$/xym6dc3MN/Tw6U9DffoPeZBC3lkSKXGNvfkq8b5l0DoLEBsP3kei', 10),
(21917771, 'CHICHE', 'ALBANE', '$2y$10$6XOM1.fRXouDurkmMGHhmulsaBdSQEjZmrHy8Z54D0QXg8.hqT61K', 9);

-- --------------------------------------------------------

--
-- Structure de la table `experiences`
--

DROP TABLE IF EXISTS `experiences`;
CREATE TABLE IF NOT EXISTS `experiences` (
  `numExp` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(150) NOT NULL,
  `dateDeb` date NOT NULL,
  `dateFin` date NOT NULL,
  `resume` varchar(500) NOT NULL,
  `etat` enum('Ouvert','Ferm?e') NOT NULL,
  `numEns` int(11) NOT NULL,
  `numSem` int(2) NOT NULL,
  PRIMARY KEY (`numExp`),
  KEY `fknumens` (`numEns`),
  KEY `fknumsemexp` (`numSem`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `experiences`
--

INSERT INTO `experiences` (`numExp`, `titre`, `dateDeb`, `dateFin`, `resume`, `etat`, `numEns`, `numSem`) VALUES
(1, 'Experience Vessie', '2020-05-24', '2020-05-25', '', 'Ouvert', 123456, 1),
(5, 'Experience Rat', '2020-06-19', '2020-06-26', '', 'Ouvert', 123456, 2);

-- --------------------------------------------------------

--
-- Structure de la table `groupestd`
--

DROP TABLE IF EXISTS `groupestd`;
CREATE TABLE IF NOT EXISTS `groupestd` (
  `numTD` int(11) NOT NULL AUTO_INCREMENT,
  `numSec` int(11) NOT NULL,
  `nomTD` varchar(15) NOT NULL,
  PRIMARY KEY (`numTD`),
  KEY `fknumsec` (`numSec`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `groupestd`
--

INSERT INTO `groupestd` (`numTD`, `numSec`, `nomTD`) VALUES
(1, 1, '1'),
(2, 1, '2'),
(4, 2, '3'),
(6, 4, '1'),
(7, 4, '2'),
(8, 4, '3'),
(9, 4, '4'),
(10, 4, '5');

-- --------------------------------------------------------

--
-- Structure de la table `groupestp`
--

DROP TABLE IF EXISTS `groupestp`;
CREATE TABLE IF NOT EXISTS `groupestp` (
  `numTP` int(11) NOT NULL AUTO_INCREMENT,
  `numTD` int(11) NOT NULL,
  `nomTP` varchar(15) NOT NULL,
  PRIMARY KEY (`numTP`),
  KEY `fknumtd` (`numTD`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `groupestp`
--

INSERT INTO `groupestp` (`numTP`, `numTD`, `nomTP`) VALUES
(1, 1, '1'),
(2, 2, '1'),
(3, 1, '2'),
(7, 4, '1'),
(8, 6, '1'),
(9, 6, '2'),
(10, 7, '1'),
(11, 7, '2'),
(12, 8, '1'),
(13, 8, '2'),
(14, 9, '1'),
(15, 9, '2'),
(16, 10, '1'),
(17, 10, '2');

-- --------------------------------------------------------

--
-- Structure de la table `remplit`
--

DROP TABLE IF EXISTS `remplit`;
CREATE TABLE IF NOT EXISTS `remplit` (
  `numCol` int(11) NOT NULL,
  `numEtu` int(11) NOT NULL,
  `donnees` varchar(250) NOT NULL,
  PRIMARY KEY (`numCol`,`numEtu`) USING BTREE,
  KEY `fknumeturemplit` (`numEtu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `remplit`
--

INSERT INTO `remplit` (`numCol`, `numEtu`, `donnees`) VALUES
(4, 21704177, '3.72'),
(4, 21707008, '3.58'),
(4, 21800368, '3.63'),
(4, 21813587, '5.04'),
(4, 21908401, '9'),
(4, 21908407, '1'),
(5, 21704177, '3.72'),
(5, 21707008, '3.54'),
(5, 21800368, '3.62'),
(5, 21813587, '5.03'),
(5, 21908401, '9'),
(5, 21908407, '2'),
(22, 21704177, '3.66'),
(22, 21707008, '3.46'),
(22, 21800368, '3.50'),
(22, 21813587, '4.96'),
(22, 21908401, '9'),
(22, 21908407, '3'),
(23, 21704177, '3.64'),
(23, 21707008, '3.42'),
(23, 21800368, '3.43'),
(23, 21813587, '4.87'),
(23, 21908401, '9'),
(23, 21908407, '4'),
(24, 21704177, '3.35'),
(24, 21707008, '3.34'),
(24, 21800368, '3.09'),
(24, 21813587, '4.51'),
(24, 21908401, '9'),
(24, 21908407, '5'),
(25, 21704177, '3.11'),
(25, 21707008, '3.20'),
(25, 21800368, '2.58'),
(25, 21813587, '4.04'),
(25, 21908401, '9'),
(25, 21908407, '6'),
(26, 21704177, '2.94'),
(26, 21707008, '3.08'),
(26, 21800368, '2.30'),
(26, 21813587, '3.51'),
(26, 21908401, '9'),
(26, 21908407, '7'),
(27, 21704177, '2.80'),
(27, 21707008, '2.98'),
(27, 21800368, '2.04'),
(27, 21813587, '2.98'),
(27, 21908401, '9'),
(27, 21908407, '8'),
(28, 21704177, '2.72'),
(28, 21707008, '2.92'),
(28, 21800368, '1.93'),
(28, 21813587, '2.74'),
(28, 21908401, '9'),
(28, 21908407, '9'),
(29, 21704177, '2.69'),
(29, 21707008, '2.89'),
(29, 21800368, '1.85'),
(29, 21813587, '2.64'),
(29, 21908401, '9'),
(29, 21908407, '10'),
(31, 20000000, '1'),
(32, 20000000, '2'),
(33, 20000000, '2'),
(35, 20000000, '5'),
(36, 20000000, '4'),
(37, 20000000, '3');

-- --------------------------------------------------------

--
-- Structure de la table `sections`
--

DROP TABLE IF EXISTS `sections`;
CREATE TABLE IF NOT EXISTS `sections` (
  `numSec` int(11) NOT NULL AUTO_INCREMENT,
  `numSem` int(2) NOT NULL,
  `nomSec` varchar(15) NOT NULL,
  PRIMARY KEY (`numSec`),
  KEY `fknumsemsec` (`numSem`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `sections`
--

INSERT INTO `sections` (`numSec`, `numSem`, `nomSec`) VALUES
(1, 1, 'A'),
(2, 1, 'B'),
(3, 1, 'C'),
(4, 2, 'A');

-- --------------------------------------------------------

--
-- Structure de la table `semestres`
--

DROP TABLE IF EXISTS `semestres`;
CREATE TABLE IF NOT EXISTS `semestres` (
  `numSem` int(2) NOT NULL,
  PRIMARY KEY (`numSem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `semestres`
--

INSERT INTO `semestres` (`numSem`) VALUES
(1),
(2),
(3),
(4),
(5),
(6);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `colonnes`
--
ALTER TABLE `colonnes`
  ADD CONSTRAINT `fknumcolmere` FOREIGN KEY (`numColMere`) REFERENCES `colonnes` (`numCol`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fknumexpcol` FOREIGN KEY (`numExp`) REFERENCES `experiences` (`numExp`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `etudiants`
--
ALTER TABLE `etudiants`
  ADD CONSTRAINT `fknumtp` FOREIGN KEY (`numTP`) REFERENCES `groupestp` (`numTP`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `experiences`
--
ALTER TABLE `experiences`
  ADD CONSTRAINT `fknumens` FOREIGN KEY (`numEns`) REFERENCES `enseignants` (`numEns`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fknumsemexp` FOREIGN KEY (`numSem`) REFERENCES `semestres` (`numSem`);

--
-- Contraintes pour la table `groupestd`
--
ALTER TABLE `groupestd`
  ADD CONSTRAINT `fknumsec` FOREIGN KEY (`numSec`) REFERENCES `sections` (`numSec`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `groupestp`
--
ALTER TABLE `groupestp`
  ADD CONSTRAINT `fknumtd` FOREIGN KEY (`numTD`) REFERENCES `groupestd` (`numTD`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `remplit`
--
ALTER TABLE `remplit`
  ADD CONSTRAINT `fknumcolremplit` FOREIGN KEY (`numCol`) REFERENCES `colonnes` (`numCol`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fknumeturemplit` FOREIGN KEY (`numEtu`) REFERENCES `etudiants` (`numEtu`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `fknumsemsec` FOREIGN KEY (`numSem`) REFERENCES `semestres` (`numSem`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
