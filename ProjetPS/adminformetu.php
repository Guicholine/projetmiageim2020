<?php
require 'fonctions.php';
session_start();
	
	$session=connexionbd();
  if (!(isset($_SESSION['typeuser']) and isset($_SESSION["login"]) and $_SESSION['typeuser']=="admin" and $_SESSION['login']=="admin")) {
  header("location:index.php"); 
}
else {
?>
<!DOCTYPE html>

<html>
<head>
 <link rel="stylesheet" media="screen" type="text/css" href="css/loginStyle.css"/>
 <meta charset="utf-8" />
<!--  <title>UT3 - Authentification </title> -->
</head>
<h3><center>Gestion étudiants</center></h3>

<div>
  <div class="form">
   <form class="login-form" method="POST" action="adminetu.php">
    <button type="submit" value="0" name="admetu">Importer des étudiants depuis un fichier</button>
    <button type="submit" value="1" name="admetu">Ajouter un étudiant</button>
    <button type="submit" value="2" name="admetu">Modifier un étudiant</button>
    <button type="submit" value="3" name="admetu">Supprimer un étudiant</button>

  </form>
  </br>
	<a href="menu.php"><input type="button" value="Retour au menu"></a>
</div>
</div>

</div>
</html>
<?php
}
?>

